import React, {useEffect} from 'react';
import {LogBox, StatusBar, StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import MainStack from './src/navigation/MainStack';
import RootNavigator from './src/navigation/RootNavigator';
import SplashScreen from 'react-native-splash-screen';


function App() {

  useEffect(() => {
    setTimeout(
      function () {
        SplashScreen.hide();
      }.bind(this),
      3000,
    );
  }, []);
  
  LogBox.ignoreAllLogs();

  return (
    <>
      <NavigationContainer>
        {/* <NotificationController/> */}
        <RootNavigator />
        {/* <MainStack /> */}
      </NavigationContainer>
    </>
  );
}

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
