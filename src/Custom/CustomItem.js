import { Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstants } from '../utils/constants'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import { Height, Width } from '../dimensions/dimensions'


const CustomItem = (props) => {

    return (
        <View style={[styles.Container, {
            backgroundColor: props.backgroundColor ? props.backgroundColor : colorConstant.white,
            marginTop: props.marginTop ? props.marginTop : moderateVerticalScale(20)
        }]}>



            <View style={styles.ContentView}>

                <Text style={{
                    fontFamily: fontConstant.bold,
                    color: colorConstant.black,
                    fontSize: 14
                }}>Booking ID <Text ></Text></Text>

                <Text style={{
                    fontFamily: fontConstant.regular,
                    color: colorConstant.yellow,
                    fontSize: 14
                }}>2193019579</Text>

            </View>


            <View style={{ width: "100%", borderBottomWidth: 1, borderBottomColor: "#E1F2FF", marginTop: moderateVerticalScale(10), }} />




            <View style={{
                flexDirection: "row",
                width: "90%",
                paddingHorizontal: "5%",
                marginTop: props.margintop ? props.margintop : moderateVerticalScale(15)
            }}>
                <Image
                    source={imageConstants.profil}
                    resizeMode='contain'
                    style={{
                        width: Width * 0.15,
                        height: Height * 0.08,
                        // backgroundColor:"pink"
                    }}
                />

                <View style={{
                    width: "90%",

                    paddingHorizontal: "5%",

                }}>

                    <Text style={{
                        fontFamily: fontConstant.bold,
                        fontSize: 14,
                        color: colorConstant.black
                    }}>Leslie Alexander</Text>

                    <View style={{ width: "100%", borderBottomWidth: 1, borderBottomColor: "#E1F2FF", marginTop: moderateVerticalScale(10), marginBottom: moderateVerticalScale(4) }} />

                    <View style={{
                        flexDirection: "row",
                        alignItems: "center",
                        width: "100%",
                        right: "10%",
                        justifyContent: "space-evenly",
                        marginTop: moderateVerticalScale(5)
                    }}>
                        <Image
                            source={props.phoneImage ? props.phoneImage : imageConstants.phone1}
                            resizeMode='contain'
                            style={{
                                width: 15,
                                height: 15
                            }}
                        />
                        <Text style={{
                            fontFamily: fontConstant.regular,
                            fontSize: 12,
                            color: props.phonecolor
                        }}>+91 1234567890</Text>
                        <Image
                            source={props.timeImage ? props.timeImage : imageConstants.Time}
                            resizeMode='contain'
                            style={{
                                width: 15,
                                height: 15,
                                 marginLeft:"8%",
                            }}
                        />
                        <Text style={{
                            fontFamily: fontConstant.regular,
                            fontSize: 12,
                            // backgroundColor:"pink",
                           
                            color: props.timecolor
                        }}>10:30 am</Text>
                    </View>
                    <Text></Text>
                </View>

                {
                    props.unpaid ? 
                        <Text style={{
                            position: "absolute",
                            right: -20,
                            // backgroundColor:"pink",
                            color: colorConstant.red,
                            fontFamily: fontConstant.medium,
                            fontSize: 10,
                            top: -10
                        }}>Un-Paid</Text>
                        :
                        <View style={{
                            flexDirection:"row",
                            alignItems:"center",
                            position: "absolute",
                            right: -30,
                            top: -10,
                            // backgroundColor:"pink",
                            justifyContent:"space-around",
                            width:"35%"
                        }}>
                        <Text style={{
                            
                            // backgroundColor:"pink",
                            color: colorConstant.green,
                            fontFamily: fontConstant.medium,
                            fontSize: 12,
                            
                            
                        }}>Paid</Text>
                        <View style={{width:"3%" , backgroundColor: colorConstant.green , borderRadius:100, height:3}}/>
                        <Text  style={{
                            
                            // backgroundColor:"pink",
                            color: colorConstant.green,
                            fontFamily: fontConstant.medium,
                            fontSize: 12,
                           
                        }}>Completed</Text>
                        </View>
                    
                }



            </View>


            <View style={{ width: "100%", borderBottomWidth: 1, borderBottomColor: "#E1F2FF", }} />


            <View style={{ flexDirection: "row", justifyContent: "space-between", alignSelf: "center", width: "90%", marginTop: moderateVerticalScale(10) }}>
                <Text style={styles.titleText}>Address</Text>
                <Text style={styles.Subtext} >Hno. 22, Bangla Society, NOIDA, Sector 70</Text>
            </View>

            <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", alignSelf: "center", width: "90%", }}>
                <View >

                    <Text style={styles.titleText}>Type of Collection</Text>
                    <Text style={styles.titleText}>WhatsApp No.</Text>
                    <Text style={styles.titleText}>Email ID</Text>
                </View>

                <View >

                    <Text style={styles.Subtext2}>Refer by Vet</Text>
                    <Text style={styles.Subtext2}>+91 1234567890</Text>
                    <Text style={styles.Subtext2}>abc@gmail.com</Text>
                </View>
            </View>



        </View>
    )
}

export default CustomItem

const styles = StyleSheet.create({
    Container: {

        width: "90%",
        // paddingHorizontal:"5%",
        paddingBottom: moderateVerticalScale(20),
        borderRadius: 18,
        borderColor: colorConstant.borderColor,
        borderWidth: 0.8,
        // height:"50%",
        alignSelf: "center",

    },

    ContentView: {
        flexDirection: "row",
        width: "90%",
        justifyContent: "space-between",
        alignItems: "center",
        alignSelf: "center",
        marginTop: moderateVerticalScale(15)
    },

    CartConatiner: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",

    },

    CartText: {
        marginTop: moderateVerticalScale(8),
        fontFamily: fontConstant.semiBold,
        color: colorConstant.profileText + "90",
        fontSize: 12

    },
    PriceText: {
        marginTop: moderateVerticalScale(8),
        fontFamily: fontConstant.semiBold,
        color: colorConstant.profileText + "90",
        fontSize: 12,
        textAlign: "right"

    },

    Subtext: {
        textAlign: "right",
        fontFamily: fontConstant.regular,
        fontSize: 13,
        lineHeight: 20,
        color: colorConstant.buttonColor,
        width: "50%"
    },
    Subtext2: {
        textAlign: "right",
        fontFamily: fontConstant.regular,
        fontSize: 13,
        lineHeight: 30,
        color: colorConstant.buttonColor
    },
    titleText: {
        fontFamily: fontConstant.bold,
        fontSize: 13,
        lineHeight: 30,
        color: colorConstant.black
    }
})