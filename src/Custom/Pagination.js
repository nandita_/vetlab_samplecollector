import { View } from "react-native"
import { Width } from "../dimensions/dimensions"
import { moderateScale } from "react-native-size-matters"
// import { s } from "react-native-wind"
// interface PaginationProps {
//     slides: Array<any>,
//     activeIndex: number
// }
const Pagination = (props) => {
    return (
        <View style={{width:Width , padding:moderateScale(5), alignItems:"center", justifyContent:"center", flexDirection:"row"}}>

            {
                [{},{},{}].map((item, index) => {
                    return (
                        
                            <View key={index}>

                                {(props.activeIndex == index)?<ActiveDot />:<Dot />}
                            </View>
                        
                    )

                })
            }
        </View>
    )
}

const ActiveDot = () => {
    return (
        <>
            <View style={{ marginLeft: 4, width: 30, height: 8, backgroundColor: '#4F4F4F', borderRadius: 20 }}></View>
        </>
    )
}
const Dot = () => {
    return (
        <>
            <View style={{ marginLeft: 4, width: 8, height: 8, backgroundColor: 'rgba(79, 79, 79, 0.2)', borderRadius: 20 }}></View>
        </>
    )
}

export default Pagination;