import { Image, SafeAreaView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant, imageConstants } from '../utils/constants'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'

const CustomHeaderText = (props) => {
  return (
    <>
    <StatusBar barStyle={'dark-content'} translucent={true}
    // backgroundColor={props.bgColor?props.bgColor:"white"}
    backgroundColor={'transparent'}
/>
    <SafeAreaView style={{backgroundColor: colorConstant.buttonColor, paddingTop:"10%", paddingBottom: moderateVerticalScale(10)}}>
      <View style={styles.headContainer}>
        <View style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          width: "90%",
          // backgroundColor: "blue"
        }}>
          <TouchableOpacity onPress={props.onPress}>
            {/* <TouchableOpacity> */}
            <Image source={props.arrowIcon}
              resizeMode='contain'
              style={{
                width: 40,
                height: 40,
                // backgroundColor:"pink"
              }}
            />
          </TouchableOpacity>


          
              <Text style={[styles.headText, {
                fontSize: props.fontSize ? props.fontSize : 16,
                fontFamily: props.fontFamily ? props.fontFamily : fontConstant.semiBold,
                // width: props.width ? props.width :"50%"
              }]}>{props.headTitle}</Text>

          

        
     <View>
      <Image />
     </View>

        </View>


      </View>
    </SafeAreaView>
    </>
  )
}

export default CustomHeaderText

const styles = StyleSheet.create({
    headContainer: {
    //   backgroundColor:"yellow",
        flexDirection: "row",
        alignItems: "center", 
        width: "100%",
        alignSelf:"center",
        justifyContent:"space-between",
        marginTop:moderateVerticalScale(20)
    },

    headText:{

      //  backgroundColor:"pink",
        color:colorConstant.white,
        // width:"50%",
        // position:"absolute",
        // justifyContent:"center",
        textAlign:"center",
        alignSelf:"center"
        
    }
})