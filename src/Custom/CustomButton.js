import {
    ActivityIndicator,
    Image,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
  } from 'react-native';
  import React from 'react';
  import {colorConstant, fontConstant, imageConstant} from '../utils/constants';
  import {Width} from '../dimensions/dimensions';
  
  const CustomButton = props => {
    return (
      <TouchableOpacity
        onPress={props.OnButtonPress}
        activeOpacity={1}
        disabled={props.disabled}
        style={[
          {...styles.buttonStyle},
          {
            backgroundColor: props.backgroundColor
              ? props.backgroundColor
              : colorConstant.buttonColor,
              height: props.height ? props.height : 50,
            width: props.width ? props.width : '90%',
            marginTop: props.marginTop ? props.marginTop : 20,
            marginRight: props.marginRight,
            borderRadius: props.borderRadius ? props.borderRadius : 25,
            bottom: props.bottom,
            borderColor: props.borderColor,
            borderWidth: props.borderWidth,
            position: props.position,
            height: props.height ? props.height : 50,
            borderColor: props.borderColor,
            borderWidth: props.borderWidth,
            marginVertical: props.marginVertical,
            marginBottom: props.marginBottom,
            paddingHorizontal: props.paddingHorizontal,
          },
        ]}>
        {props.isLoading ? (
          <ActivityIndicator size={25} color={colorConstant.white} />
        ) : (
          <Text
            style={[
              styles.text,
              {
                color: props.color ? props.color : colorConstant.buttonText,
                fontFamily: props.fontFamily
                  ? props.fontFamily
                  : fontConstant.medium,
              },
            ]}>
            {props.buttonText}
          </Text>
        )}
      </TouchableOpacity>
    );
  };
  
  export default CustomButton;
  
  const styles = StyleSheet.create({
    buttonStyle: {
      alignSelf: 'center',
      alignItems: 'center',
      justifyContent: 'center',
      
  
      flexDirection: 'row',
    },
    text: {
      fontSize: 16,
    },
  });
  