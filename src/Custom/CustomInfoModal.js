import {
    StyleSheet,
    Text,
    View,
    Modal,
    TouchableOpacity,
    Image,
    Platform,
  } from 'react-native';
  import React, {useState} from 'react';
  import CustomButton from './CustomButton';
  import {colorConstant, fontConstant, imageConstant, imageConstants} from '../utils/constants';
  import {moderateScale, moderateVerticalScale} from 'react-native-size-matters';
  import {Height, Width} from '../dimensions/dimensions';
  
  
  const CustomInfoModal = props => {
    const toggleInfoModal = () => {
      props?.setInfoModalVisible(!props.isInfoModalVisible);
    };
    return (
      <View style={styles.centeredView}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={props.isInfoModalVisible}
          onRequestClose={() => {
            // alert('Modal has been closed.');
            props.setInfoModalVisible(!props.isInfoModalVisible);
           
          }}>

            
          <View
            style={{height: Height, backgroundColor: '#00000080'}}>
              
              <View style={styles.container}>
                <TouchableOpacity onPress={() => toggleInfoModal()} activeOpacity={0.5}
                style={{
                  position:"absolute",
                  right:-5,
                  zIndex:100,
                  bottom:"104%"
                }}
                >
              <Image 
              source={imageConstants.cross}
              resizeMode='contain'
              style={{
                width:25,
                height:25,
               
              }}
              />
              </TouchableOpacity>
            {
              props.contentTrue ? 
              <>
              <Text style={{
                fontFamily: fontConstant.medium,
                fontSize:14, 
                
                color: colorConstant.black
            }}>View instructions</Text>
          <Text
          onPress={() => toggleInfoModal()}
          style={{
            fontFamily: fontConstant.regular,
            fontSize:14,
            lineHeight:20,
            marginTop: moderateVerticalScale(8),
            color: colorConstant.black
            // width:"80%"
          }}>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).t is a lo</Text>
          </>
          :
          <View
           style={{
            alignSelf:"center",
          }}>
          <Image source={imageConstants.IDCard1}
          resizeMode='contain'
          style={{
            width: Width*0.90,
            height: Height*0.25,
            
          }}
          />
           <Image source={imageConstants.IDCard2}
          resizeMode='contain'
          style={{
            width: Width*0.90,
            height: Height*0.25,
            marginTop: moderateVerticalScale(20)
            
          }}
          />
          </View>


}
</View>
                
           
          </View>
        </Modal>
      </View>
    );
  };
  
  export default CustomInfoModal;
  
  const styles = StyleSheet.create({
  
   
  
    container: {
      position: 'absolute',
      top: "25%",
      padding: moderateVerticalScale(15),
    //   bottom: moderateVerticalScale(0),
      backgroundColor:colorConstant.white,
      borderRadius:20,
      width: '90%',
    //   alignItems: 'center',
      alignSelf: 'center',
      justifyContent: 'center',
      ...Platform.select({
        ios: {
          height: moderateVerticalScale(370),
        },
      }),
    },
  
    headText: {
      fontSize: 22,
      marginTop: moderateVerticalScale(10),
      textAlign: 'center',
      fontFamily: fontConstant.semiBold,
      color: colorConstant.textColor,
    },
  
    text2: {
      fontSize: 15,
      lineHeight: 20,
      bottom: 20,
      width: Width * 0.9,
      paddingVertical: moderateVerticalScale(10),
      marginTop: moderateVerticalScale(40),
      textAlign: 'center',
      fontFamily: fontConstant.regular,
      color: colorConstant.subTextColor,
    },
  });
  