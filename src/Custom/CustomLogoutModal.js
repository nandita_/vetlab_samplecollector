import { StyleSheet, Text, View, Modal, TouchableOpacity, Image } from 'react-native'
import React, { useState } from 'react'
import CustomButton from './CustomButton';
import { colorConstant, fontConstant, imageConstant, imageConstants } from '../utils/constants';
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters';
import { Height, Width } from '../dimensions/dimensions';

const CustomLogoutModal = (props) => {
    const toggleModal = () => {
        props.setModalVisible(!props.isModalVisible);
    };
    return (
        // <View style={styles.centeredView}>
        <Modal
            animationType="slide"
            transparent={true}
            visible={props.isModalVisible}
            onRequestClose={() => {
                // alert('Modal has been closed.');
                props.setModalVisible(!props.isModalVisible);
            }}
        >
            <View style={{ flex: 1, backgroundColor: 'rgba(126,126,126,0.54)', alignItems: 'center', justifyContent: 'center' }}>
                <View style={styles.container}>


                    <Image source={imageConstants.logoutModal} resizeMode='contain' style={{
                        width:80, 
                        height:80,
                        // backgroundColor:"pink",
                        marginTop: moderateVerticalScale(20)
                    }}/>
                    <Text style={styles.headText}>{props.headerText}</Text>


                    {/* <Text style={styles.text2}>{props.subText}</Text> */}
                    <View style={{
                        // backgroundColor: "pink",
                        width: "95%",
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "space-evenly",
                        paddingBottom: moderateVerticalScale(10),
                        marginTop: moderateVerticalScale(20)
                        
                    }}>


                        <CustomButton
                            OnButtonPress={props.OnButtonCancel}
                            buttonText={props.cancelText}
                            height={45}
                            // position={'absolute'}
                            width={"45%"}
                            color={colorConstant.red}
                            backgroundColor={"#FFF4F4"}
                            bottom={10}
                            marginTop={20}
                        // bottom={props.bottom}
                        />

                        <CustomButton
                            OnButtonPress={props.OnButtonDelete}
                            buttonText={props.buttonText}
                            backgroundColor={colorConstant.red}
                            color={colorConstant.white}
                            // position={'absolute'}
                            width={"45%"}
                            height={45}
                            bottom={10}
                            marginTop={20}
                        // bottom={props.bottom}
                        />
                    </View>

                </View>
            </View>
        </Modal>
        // </View>

    )
}

export default CustomLogoutModal

const styles = StyleSheet.create({
    centeredView: {
        // flex: 1,
        backgroundColor: 'rgba(126,126,126,0.54)'        // position:"absolute",
        // width:"100%",
        // top: moderateVerticalScale(70),
        // bottom: moderateVerticalScale(70),
        // justifyContent: 'center',
        // backgroundColor:"pink",
        // alignItems: 'center',
        // marginTop: 22,
    },
    modalView: {
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },

    image: {
        width: 130,
        height: 130,
        bottom: moderateScale(60)
    },

    container: {
        backgroundColor: colorConstant.white,
        borderRadius: 20,
        // top: moderateVerticalScale(150),
        width: "90%",
        // height:"10%",
        // borderWidth:1,
        alignItems: "center",
        alignSelf: "center",
        justifyContent: "center",
        // minHeight:Height*0.55

    },

    headText: {
        fontSize: 20,
        marginTop: moderateVerticalScale(20),
        // bottom:40,
        // lineHeight:28,
        textAlign: "center",
        // backgroundColor:"pink",
        // width:Width*0.90,
        // bottom
        fontFamily: fontConstant.semiBold,
        color: colorConstant.black
    },

    text2: {
        fontSize: 18,
        // lineHeight:25,
        // bottom:20,
        marginTop: moderateVerticalScale(20),
        width: Width * 0.90,
        textAlign: "center",
        fontFamily: fontConstant.semiBold,
        // fontWeight:"600",
        color: colorConstant.textColor
    },

    sizeText: {
        fontFamily: fontConstant.regular,
        color: colorConstant.textColor,
        fontSize: 14,
        lineHeight: 28
    }
})