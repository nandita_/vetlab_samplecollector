import {
    StyleSheet,
    Text,
    View,
    Modal,
    TouchableOpacity,
    Image,
    Platform,
    FlatList,
    TextInput,
} from 'react-native';
import React, { useEffect, useState } from 'react';
import CustomButton from './CustomButton';
import { colorConstant, fontConstant, imageConstant, imageConstants, shadowObject } from '../utils/constants';
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters';
import { Height, Width } from '../dimensions/dimensions';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';



const CustomCancelModal = props => {

    const toggleModal = () => {
        props?.setModalVisible(!props.isModalVisible);
    };

    const [active, setActive] = useState(false)
    const paymentdata = [
        {
            id: 1,
            title: "Customer did not pick the call"
        },
        {
            id: 2,
            title: "Address In correct"
        },
        {
            id: 3,
            title: "Customer Refused to take sample test"
        },
        {
            id: 4,
            title: "Others"
        }
    ]

    // console.log(active , "dataaa");
    const handlePress = (index) => {
        setActive(index === active ? -1 : index);
    };

    const renderItem = ({ item, index }) => {
        // console.log(index , "check index");

        return (
            <View style={styles.innerContainer} >
                <View style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                    width: "100%",
                    alignSelf: "center",
                    // paddingBottom:10
                    //    marginTop:10
                }}>
                    <Text style={{
                        fontFamily: fontConstant.medium,
                        fontSize: 14,
                        color: colorConstant.black
                    }}>{item.title}</Text>
                    <TouchableOpacity onPress={() => handlePress(index)}>
                        <Image
                            source={index === active ? imageConstants.checkbutton : imageConstants.uncheckbutton}
                            resizeMode='contain'
                            style={{
                                width: 25,
                                height: 25
                            }}
                        />
                    </TouchableOpacity>
                </View>
                <View style={{ width: "100%", borderBottomColor: "#DBDBDB", borderBottomWidth: 1, alignSelf: "center", paddingBottom: 10 }} />
            </View>
        )
    }
    return (
        // <KeyboardAwareScrollView
        // extraHeight={moderateScale(100)}
        // contentContainerStyle={{
        //     // paddingBottom
        // }}>
        <View style={styles.centeredView}>
            <Modal
                animationType="slide"

                transparent={true}
                visible={props.isModalVisible}
                onRequestClose={() => {
                    // alert('Modal has been closed.');
                    props.setModalVisible(!props.isModalVisible);

                }}>
                     
                                   
                <TouchableOpacity
                activeOpacity={0.5}
                onPress={() => toggleModal()}
                    style={{ height: Height, backgroundColor: '#00000080' , flex:1, justifyContent:"flex-end"}}>
                       
                    <View style={styles.container}>
                        {/* <Text style={styles.cancelText}>Cancel Booking</Text> */}
                        <Text style={[styles.selectReason, {
                            top: active === 3 ? "8%" : "10%",
                        }]}>Please select the reason for cancellations:</Text>


                        <FlatList
                        showsVerticalScrollIndicator={false}
                            data={paymentdata}
                            renderItem={renderItem}
                            contentContainerStyle={{
                                marginTop: moderateVerticalScale(60)
                            }}
                        />

                        {
                            active === 3 && (
                                <View style={{
                                    paddingBottom: moderateVerticalScale(20)
                                }}>
                                        <Text style={styles.OtherText}>Others</Text>

                                        <TextInput
                                            style={styles.reasonInput}
                                            placeholder='Describe issue'
                                            placeholderTextColor={colorConstant.secondaryText}
                                        />

                                    </View>
                                
                            )
                        }


                        <CustomButton
                            color={"white"}
                            OnButtonPress={
                                props.OnButtonPress ? props.OnButtonPress : toggleModal
                            }
                            buttonText={"Cancel Appointment"}
                            width={'90%'}
                            bottom={10}
                        />
                    </View>
                
                </TouchableOpacity>
            </Modal>
        </View>
        // </KeyboardAwareScrollView>
    );
};

export default CustomCancelModal;

const styles = StyleSheet.create({

   

    container: {
        // flex:1,
        // justifyContent:"flex-end",
        // position: 'absolute',
        // bottom: moderateVerticalScale(0),
        backgroundColor: "#EDF7FF",
        width: '100%',
        padding: moderateVerticalScale(10),
        //   alignItems: 'center',
        alignSelf: 'center',

        // justifyContent: 'center',
        ...Platform.select({
            ios: {
                height: moderateVerticalScale(370),
            },
            android: {
                minHeigth: moderateVerticalScale(400),
                maxHeight: moderateVerticalScale(600)
            }
        }),
    },

    cancelText: {
        fontFamily: fontConstant.medium,
        fontSize: 16,
        color: colorConstant.black,
        position: "absolute",
        top: "5%",
        paddingHorizontal: moderateScale(15)
    },

    selectReason: {
        fontFamily: fontConstant.medium,
        fontSize: 14,
        color: colorConstant.black,
        position: "absolute",
        // top: "5%",
        paddingHorizontal: moderateScale(15)
    },

    headText: {
        fontSize: 22,
        marginTop: moderateVerticalScale(10),
        textAlign: 'center',
        fontFamily: fontConstant.semiBold,
        color: colorConstant.textColor,
    },

    text2: {
        fontSize: 15,
        lineHeight: 20,
        bottom: 20,
        width: Width * 0.9,
        paddingVertical: moderateVerticalScale(10),
        marginTop: moderateVerticalScale(40),
        textAlign: 'center',
        fontFamily: fontConstant.regular,
        color: colorConstant.subTextColor,
    },

    innerContainer: {
        padding: moderateVerticalScale(8),
        marginTop: moderateVerticalScale(5),

    },

    OtherText: {
        fontFamily: fontConstant.medium,
        fontSize: 16,
        color: colorConstant.black,
        // position:"absolute",
        // top:"5%",
        // marginTop: moderateVerticalScale(-10),
        paddingHorizontal: moderateScale(8)
    },

    reasonInput: {
        backgroundColor: "#F8FCFF",
        width: "95%",
        alignSelf: "center",
        marginTop: moderateVerticalScale(10),
        //    paddingBottom: moderateVerticalScale(20),
        borderRadius: 30,
        padding: moderateScale(10),
        // ...shadowObject
    }
});
