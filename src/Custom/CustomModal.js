import {
    StyleSheet,
    Text,
    View,
    Modal,
    TouchableOpacity,
    Image,
    Platform,
  } from 'react-native';
  import React, {useState} from 'react';
  import CustomButton from './CustomButton';
  import {colorConstant, fontConstant, imageConstant, imageConstants} from '../utils/constants';
  import {moderateScale, moderateVerticalScale} from 'react-native-size-matters';
  import {Height, Width} from '../dimensions/dimensions';
  
  
  const CustomModal = props => {
    const toggleModal = () => {
      props?.setModalVisible(!props.isModalVisible);
    };
    return (
      <View style={styles.centeredView}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={props.isModalVisible}
          onRequestClose={() => {
            // alert('Modal has been closed.');
            props.setModalVisible(!props.isModalVisible);
           
          }}>
          <View
            style={{height: Height, backgroundColor: '#00000080'}}>
            <View style={styles.container}>
              <Image
                source={imageConstants.vacination}
                resizeMode="contain"
                style={styles.image}
              />
              
  
              <CustomButton
                color={"white"}
                OnButtonPress={
                  props.OnButtonPress ? props.OnButtonPress : toggleModal
                }
                buttonText={"Upload Sample UID Image"}
                width={'90%'}
                bottom={20}
              />
            </View>
          </View>
        </Modal>
      </View>
    );
  };
  
  export default CustomModal;
  
  const styles = StyleSheet.create({
  
    image: {
      width: 370,
      height: Height*0.30,
      marginTop: moderateVerticalScale(20),
    },
  
    container: {
      position: 'absolute',
      bottom: moderateVerticalScale(0),
      backgroundColor: "#EDF7FF",
      width: '100%',
      alignItems: 'center',
      alignSelf: 'center',
      justifyContent: 'center',
      ...Platform.select({
        ios: {
          height: moderateVerticalScale(370),
        },
      }),
    },
  
    headText: {
      fontSize: 22,
      marginTop: moderateVerticalScale(10),
      textAlign: 'center',
      fontFamily: fontConstant.semiBold,
      color: colorConstant.textColor,
    },
  
    text2: {
      fontSize: 15,
      lineHeight: 20,
      bottom: 20,
      width: Width * 0.9,
      paddingVertical: moderateVerticalScale(10),
      marginTop: moderateVerticalScale(40),
      textAlign: 'center',
      fontFamily: fontConstant.regular,
      color: colorConstant.subTextColor,
    },
  });
  