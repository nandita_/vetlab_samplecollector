import { Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstants } from '../utils/constants'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import { Height, Width } from '../dimensions/dimensions'


const CustomTotal = (props) => {

    return (
        <View style={[styles.Container, {
            backgroundColor: props.backgroundColor ? props.backgroundColor : colorConstant.white,
            marginTop: props.marginTop ? props.marginTop : moderateVerticalScale(20)
        }]}>




            <View style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
                width: "90%",
                alignSelf: "center",
                marginTop: moderateVerticalScale(10)
            }}>

                <Text style={styles.titleText}>{props.title1}</Text>
                <Text style={styles.Subtext2}>₹ 1100</Text>
            </View>

            <View style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
                width: "90%",
                alignSelf: "center",
                marginTop: moderateVerticalScale(10)
            }}>

                <Text style={styles.titleText}>{props.title2}</Text>
                <Text style={styles.Subtext2}>₹ 1100</Text>
            </View>

            <View style={{
                flexDirection: "row",
                // alignItems:"center",
                justifyContent: "space-between",
                width: "90%",
                alignSelf: "center",
                marginTop: moderateVerticalScale(10)
            }}>

                <Text style={styles.titleText}>{props.title3}</Text>
                <Text style={styles.Subtext2}>₹ 1100</Text>
            </View>

            <View style={{
                flexDirection: "row",
                // alignItems:"center",
                justifyContent: "space-between",
                width: "90%",
                alignSelf: "center",
                marginTop: moderateVerticalScale(10)
            }}>

                <Text style={styles.titleText}>{props.title4}</Text>
                <Text style={[styles.Subtext2, {
                    color: props.color ? props.color : colorConstant.black
                }]}>₹ 1100</Text>
            </View>

            <View style={{ width: "92%", borderBottomWidth: 1, borderBottomColor: "#DBDBDB", marginTop: moderateVerticalScale(10), alignSelf: "center" }} />


            <View style={{
                flexDirection: "row",
                // alignItems:"center",
                justifyContent: "space-between",
                width: "90%",
                alignSelf: "center",
                marginTop: moderateVerticalScale(10)
            }}>

                <Text style={[styles.titleText, { color: colorConstant.black, fontFamily: fontConstant.bold }]}>Total</Text>
                <Text style={styles.Subtext2}>₹ 1100</Text>
            </View>

            {
                props.payment && (
                    <>
                        <View style={{ width: "92%", borderBottomWidth: 1, borderBottomColor: "#DBDBDB", marginTop: moderateVerticalScale(10), alignSelf: "center" }} />

                        <View style={{
                            flexDirection: "row",
                            // alignItems:"center",
                            justifyContent: "space-between",
                            width: "90%",
                            alignSelf: "center",
                            marginTop: moderateVerticalScale(10)
                        }}>

                            <Text style={[styles.titleText, { color: colorConstant.black, fontFamily: fontConstant.bold }]}>Payment</Text>
                            <Text style={styles.Subtext2}>UPI</Text>
                        </View>
                    </>
                )
            }

        </View>
    )
}

export default CustomTotal

const styles = StyleSheet.create({
    Container: {

        width: "90%",
        // paddingHorizontal:"5%",
        paddingBottom: moderateVerticalScale(20),
        borderRadius: 18,
        borderColor: colorConstant.borderColor,
        borderWidth: 0.8,
        // height:"50%",
        alignSelf: "center",

    },

    ContentView: {
        flexDirection: "row",
        width: "90%",
        justifyContent: "space-between",
        alignItems: "center",
        alignSelf: "center",
        marginTop: moderateVerticalScale(15)
    },

    CartConatiner: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",

    },

    CartText: {
        marginTop: moderateVerticalScale(8),
        fontFamily: fontConstant.semiBold,
        color: colorConstant.profileText + "90",
        fontSize: 12

    },
    PriceText: {
        marginTop: moderateVerticalScale(8),
        fontFamily: fontConstant.semiBold,
        color: colorConstant.profileText + "90",
        fontSize: 12,
        textAlign: "right"

    },


    Subtext2: {
        textAlign: "right",
        fontFamily: fontConstant.medium,
        fontSize: 13,
        color: colorConstant.black
    },
    titleText: {
        fontFamily: fontConstant.medium,
        fontSize: 13,
        lineHeight: 22,
        width: "70%",
        color: colorConstant.buttonColor
    }
})