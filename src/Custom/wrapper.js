import { useDrawerProgress } from "@react-navigation/drawer";
import { useEffect } from "react";
import { View } from "react-native";
import Animated, { interpolate, useAnimatedStyle } from "react-native-reanimated";
import { moderateScale, moderateVerticalScale } from "react-native-size-matters";
import { Height } from "../dimensions/dimensions";
import { colorConstant } from "../utils/constants";

const Wrapper = (props) => {
    const progress = useDrawerProgress();
    // console.log(progress.value, "Progressssss");
    const viewStyle = useAnimatedStyle(() => {
        const scale = interpolate(progress.value, [0, 1], [1, 0.8]);
        const borderRadius = interpolate(progress.value, [0, 1], [0, 20]);
        const borderWidth = interpolate(progress.value, [0, 1], [0, 0.8] )
        return {
            transform: [{ scale: scale }],
            borderRadius: borderRadius,
            borderWidth: borderWidth
        }
    })
    // const scale = interpolate(progress, 
    //     [0, 1],
    //    [1, 0.8],
    // );
    // const borderRadius = interpolate(progress, 
    //     [0, 1],
    //     [0, 20],
    // );
    useEffect(() => {
        console.log(progress.value, 'valueeee');
    })

    // const animatedStyle = useAnimatedStyle(() => {
    //     return {
    //       transform: [{ scale }],
    //       borderRadius,
    //       overflow: 'hidden',
    //     };
    //   });
    return (
        <>
            <Animated.View style={[{ flex: 1, overflow: 'hidden',borderColor: colorConstant.white }, {zIndex:999}, viewStyle]}>
                {props.children}
                </Animated.View>
            
            
<View
        style={{
          height: Height - moderateScale(260),
          width: 20,
          position: 'absolute',
          left: moderateScale(19),
          marginTop: moderateScale(120),
          borderBottomLeftRadius: 30,
          borderTopLeftRadius: 30,
          justifyContent: 'center',
          backgroundColor: 'rgba(255, 255, 255, 0.50)',
          alignItems: 'center',
          borderColor: '#000000' + 20,
          borderWidth: 1,

          // borderRadius: 100,
        }}>
        <View
          style={{
            width: 4,
            height: 70,
            backgroundColor: 'white',
            borderRadius: 9,
          }}></View>
      </View>
                
                
      
        </>
    )
}
export default Wrapper;