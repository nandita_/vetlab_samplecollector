import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant } from '../utils/constants'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import { Width } from '../dimensions/dimensions'

const CustomText = (props) => {
  return (
    <View style={[styles.container, {
      marginTop: props.marginTop ? props.marginTop : moderateVerticalScale(40)
    }]}>
      <Text style={[styles.TitleStyle, {
        fontFamily:props.fontFamily? props.fontFamily: fontConstant.medium,
        color: props.color ? props.color: colorConstant.black,
        fontSize: props.fontSize ? props.fontSize : 16,
        paddingHorizontal: props.paddingHorizontal
      }]}>{props.Title}</Text>
    </View>
  )
}

export default CustomText

const styles = StyleSheet.create({
    container:{
     width: "90%",
     alignSelf:"center",
     
    },
    
})