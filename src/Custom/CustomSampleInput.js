import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import React, {useState} from 'react';
import {colorConstant, fontConstant, imageConstants} from '../utils/constants';
import {
  moderateScale,
  moderateVerticalScale,
  scale,
} from 'react-native-size-matters';
import {Width} from '../dimensions/dimensions';
import {Dropdown} from 'react-native-element-dropdown';
import CustomInput from './CustomInput';
import {shadowObject} from '../utils/constants';

const CustomSampleInput = props => {
  const [value, setValue] = useState('');
  const [isFocus, setIsFocus] = useState(false);
  const [status, setStatus] = useState(false);
  // const [active , setActive] = useState(false);
  const [reasonOtherFlag, setReasonOtherFlag] = useState('');

  const [reasonStatus, setReasonStatus] = useState(false);

  const data = [
    {label: 'EDTA Blood', value: 'reson1'},
    {label: 'Serum', value: 'reason2'},
    {label: 'Plasma', value: 'reason3'},
    {label: 'Urine', value: 'reason4'},
    {label: 'Stool', value: 'reason5'},
    {label: 'Fluid', value: 'reason6'},
    {label: 'Tissue', value: 'reason7'},
    {label: 'Milk', value: 'reason8'},
    {label: 'Eye discharge', value: 'reason9'},
    {label: 'Nasal discharge', value: 'reason10'},
    {label: 'Others', value: 'reason11'},
  ];

  //   {Type of samples - In place of reason it would be - EDTA Blood- Serum-Plasma Urine-Stool-Fluid- Tissue- Milk- Eye discharge- Nasal discharge- Any other (Type)}

  return (
    <View
      style={[
        styles.Container,
        {
          backgroundColor: props.backgroundColor
            ? props.backgroundColor
            : colorConstant.white,
          marginTop: props.marginTop
            ? props.marginTop
            : moderateVerticalScale(20),
        },
      ]}>
      <View style={styles.ContentView}>
        <Text
          style={{
            fontFamily: fontConstant.bold,
            color: colorConstant.buttonColor,
            fontSize: 14,
          }}>
          Test Name<Text></Text>
        </Text>

        <Text
          style={{
            fontFamily: fontConstant.regular,
            color: colorConstant.black,
            fontSize: 14,
          }}>
          Rabbies
        </Text>
      </View>

      <View style={styles.ContentView}>
        <Text
          style={{
            fontFamily: fontConstant.bold,
            color: colorConstant.buttonColor,
            fontSize: 14,
          }}>
          Test Price<Text></Text>
        </Text>

        <Text
          style={{
            fontFamily: fontConstant.regular,
            color: colorConstant.black,
            fontSize: 14,
          }}>
          ₹ 1100
        </Text>
      </View>

      <Text
        style={{
          fontFamily: fontConstant.bold,
          fontSize: 14,
          color: colorConstant.black,
          paddingHorizontal: moderateVerticalScale(18),
          marginTop: moderateVerticalScale(15),
        }}>
        Type of Sample Collected
      </Text>

      <View style={styles.DropView}>
        <Dropdown
          itemTextStyle={{
            color: colorConstant.textColor,
            fontSize: 14,
            fontFamily: fontConstant.regular,
          }}
          data={data}
          containerStyle={{
            borderRadius: 10,
            padding: 8,
            paddingHorizontal: 10,
          }}
          style={styles.dropdown}
          placeholderStyle={styles.placeholderStyle}
          selectedTextStyle={styles.selectedTextStyle}
          // data={serviceData}
          placeholder={props.placeholder}
          // placeholder='EDAT Blood'
          maxHeight={160}
          labelField="label"
          valueField="value"
          value={value}
          onFocus={() => setIsFocus(true)}
          onBlur={() => setIsFocus(false)}
          onChange={item => {
            setValue(item.value);
            console.log(item.value, 'huihui');
            setIsFocus(false);
            if (item.value == 'reason11') {
              setStatus(true);
              setReasonStatus(true);
            } else {
              setStatus(false);
              setReasonStatus(false);
            }
          }}
          // onChange={item => setReasonOtherFlag(item.value)}
          // renderItem={renderItem}
        />
        {status && (
          <TextInput
            placeholder="other"
            placeholderTextColor={colorConstant.black}
            style={{
              borderWidth: 1,
              borderRadius: 16,
              borderColor: colorConstant.borderColor,
              paddingLeft: scale(15),
              marginTop: moderateVerticalScale(10),
            }}
          />
        )}
      </View>
      {props.reasonFlag ? (
        reasonStatus && (
          <>
            <Text
              style={{
                fontFamily: fontConstant.bold,
                fontSize: 14,
                color: colorConstant.black,
                paddingHorizontal: moderateVerticalScale(18),
                marginTop: moderateVerticalScale(15),
              }}>
              Resaon
            </Text>

            <TextInput
              style={styles.reasonInput}
              placeholder="Enter Reason"
              placeholderTextColor={colorConstant.secondaryText}
            />
          </>
        )
      ) : (
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            // backgroundColor:"pink",
            width: '95%',
          }}>
          <Text
            style={{
              fontFamily: fontConstant.bold,
              fontSize: 14,
              color: colorConstant.black,
              paddingHorizontal: moderateVerticalScale(18),
              marginTop: moderateVerticalScale(15),
            }}>
            Capture Sample UID
          </Text>

          <TouchableOpacity onPress={props.infoModal}>
            <Image
              source={imageConstants.info}
              resizeMode="contain"
              style={{
                width: 20,
                height: 20,
                marginTop: moderateVerticalScale(15),
              }}
            />
          </TouchableOpacity>
        </View>
      )}

      {!props.reasonFlag && (
        <TouchableOpacity
          style={styles.DocContainer}
          activeOpacity={0.8}
          onPress={props.onPress}>
          <Image
            source={imageConstants.Icon}
            resizeMode="contain"
            style={{
              width: 20,
              height: 20,
              alignSelf: 'center',
            }}
          />

          <Text style={styles.DocText}>Upload Image</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

export default CustomSampleInput;

const styles = StyleSheet.create({
  Container: {
    width: '90%',
    // paddingHorizontal:"5%",
    paddingBottom: moderateVerticalScale(20),
    borderRadius: 18,
    borderColor: colorConstant.borderColor,
    borderWidth: 0.8,
    // height:"50%",
    alignSelf: 'center',
  },

  ContentView: {
    flexDirection: 'row',
    width: '90%',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: moderateVerticalScale(15),
  },

  DropView: {
    marginTop: 15,
    alignSelf: 'center',
    width: '90%',
  },

  dropdown: {
    width: Width * 0.8,
    borderRadius: 15,
    borderWidth: 0.8,
    backgroundColor: colorConstant.white,
    borderColor: colorConstant.borderColor,
    paddingVertical: Platform.OS === 'ios' ? 5 : 10,
    // height: moderateVerticalScale(50),
    padding: moderateScale(15),
    fontSize: 16,
    fontFamily: fontConstant.semiBold,
    // color:"red",
    shadowOffset: {
      width: 0,
      height: 1,
    },
  },

  placeholderStyle: {
    fontSize: 14,
    fontFamily: fontConstant.medium,
    color: colorConstant.secondaryText,
    // color:"black"
  },

  selectedTextStyle: {
    fontSize: 15,
    // paddingVertical: 5,
    // bottom:10,
    fontFamily: fontConstant.semiBold,
    color: colorConstant.black,
  },

  DocContainer: {
    backgroundColor: '#EFF4FF',
    width: '90%',
    height: moderateVerticalScale(70),
    marginTop: moderateVerticalScale(20),
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    borderColor: '#0174CC',
    borderWidth: 0.5,
    flexDirection: 'row',
    alignItems: 'center',
  },

  DocText: {
    fontFamily: fontConstant.bold,
    fontSize: 14,
    paddingHorizontal: moderateScale(10),
    color: colorConstant.black,
  },

  reasonInput: {
    backgroundColor: '#F8FCFF',
    width: '90%',
    alignSelf: 'center',
    marginTop: moderateVerticalScale(15),
    borderRadius: 18,
    padding: moderateScale(20),
    ...shadowObject,
  },
});
