import { Image, SafeAreaView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant, imageConstants } from '../utils/constants'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'

const CustomHeader = (props) => {
  return (
    <>
    <StatusBar barStyle={'dark-content'} translucent={true}
    // backgroundColor={props.bgColor?props.bgColor:"white"}
    backgroundColor={'transparent'}
/>
    <SafeAreaView style={{backgroundColor: colorConstant.buttonColor, paddingTop:"10%", paddingBottom: moderateVerticalScale(10)}}>
      
      <View style={styles.headContainer}>
        <View style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          width: "70%",
          // backgroundColor: "blue"
        }}>
          <TouchableOpacity onPress={props.onPress}>
            {/* <TouchableOpacity> */}
            <Image source={props.arrowIcon}
              resizeMode='contain'
              style={{
                width: 40,
                height: 40,
                // backgroundColor:"pink"
              }}
            />
          </TouchableOpacity>


          {
            props.headerImage ?
              <Image source={props.Logo}
                resizeMode='contain'
                style={{
                  width: "50%",
                  height: 50,
                  alignSelf: "center"
                }}
              />
              :
              <Text style={[styles.headText, {
                fontSize: props.fontSize ? props.fontSize : 16,
                fontFamily: props.fontFamily ? props.fontFamily : fontConstant.medium,
                // width: props.width ? props.width :"50%"
              }]}>{props.headTitle}</Text>

          }
        </View>

        {/* {
          props.notifyImage && */}
        <TouchableOpacity onPress={props.NotifyButton}>
          <Image source={props.notify}
            resizeMode='contain'
            style={{
              width: 24,
              height: 24,

            }}
          />
        </TouchableOpacity>
        {/* } */}




      </View>
    </SafeAreaView>
    </>
  )
}

export default CustomHeader

const styles = StyleSheet.create({
    headContainer: {
      // backgroundColor:"yellow",
        flexDirection: "row",
        alignItems: "center", 
        width: "90%",
        alignSelf:"center",
        justifyContent:"space-between",
        marginTop:moderateVerticalScale(20)
    },

    headText:{

      //  backgroundColor:"pink",
        color:colorConstant.black,
        // width:"50%",
        
        textAlign:"center",
        alignSelf:"center"
        
    }
})