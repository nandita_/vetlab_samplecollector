import {Image, Platform, StyleSheet, Text, TextInput, View} from 'react-native';
import React from 'react';
import {colorConstant, fontConstant} from '../utils/constants';
import {moderateScale, moderateVerticalScale} from 'react-native-size-matters';

const CustomInput = props => {
  return (
    <View
      style={[
        styles.inputView,
        {
          width: props.width ? props.width : '90%',
          height: props.height,
          marginTop: props.margin ? props.margin : 20,
          backgroundColor: props.backgroundColor
            ? props.backgroundColor
            : colorConstant.inputBackground,
          borderColor: props.borderColor,  
          borderWidth: props.borderWidth,
          borderRadius: props.borderRadius ? props.borderRadius : 25,
          paddingHorizontal: props.padding ? props.padding : moderateScale(20),
          
        },
      ]}>
      <Image
        source={props.source}
        resizeMode="contain"
        style={{
          width: 24,
          height: 24,
          // backgroundColor:"pink"
        }}
      />
      <TextInput
        value={props.value}
        multiline={props.multiline}
        editable={props.editable}
        keyboardType={props.keyboardType}
        onChangeText={props.onChangeText}
        secureTextEntry={props.secureTextEntry}
        style={[
          styles.inputStyle,
          {
            fontSize: props.fontSize ? props.fontSize : 14,
            color: colorConstant.black,
            marginTop: props.marginTop,
            paddingHorizontal: props.paddingHorizontal ? props.paddingHorizontal : moderateVerticalScale(10),
            height: props.height ? props.height : moderateVerticalScale(46),
            padding: props.padding ? props.padding : moderateScale(3),
            textAlignVertical: props.textAlignVertical,
            paddingTop: props.paddingTop,
          },
        ]}
        // ={props.value}
        placeholder={props.placeholder ? props.placeholder : 'Enter'}
        placeholderTextColor={
          props.placeholderTextColor ? props.placeholderTextColor : '#A09C90'
        }
      />

      
    </View>
  );
};

export default CustomInput;

const styles = StyleSheet.create({
  inputView: {
    flexDirection: 'row',
    // borderWidth: 1,
    alignItems: 'center',
    // borderColor: colorConstant.borderColor,
    alignSelf: 'center',
    marginRight: moderateScale(5),
    
  },
  inputStyle: {
    paddingVertical: 0,
    width: '70%',
    // backgroundColor:"pink",
    fontFamily: fontConstant.regular,
    // color:colorConstant.black
  },
});
