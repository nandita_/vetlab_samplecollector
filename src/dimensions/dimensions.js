import { Dimensions,StatusBar } from "react-native";
// import DeviceInfo from 'react-native-device-info';
const Width =  Dimensions.get('window').width;
const Height =  Dimensions.get('window').height;
export const DeviceWidth =  Dimensions.get('window').width;
export const DeviceHeight =  Dimensions.get('window').height;

const STATUS_BAR_HEIGHT = StatusBar.currentHeight;
console.log("DeviceWidth",DeviceWidth);
export {Width,Height,STATUS_BAR_HEIGHT} 