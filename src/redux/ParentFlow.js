
import { createSlice } from '@reduxjs/toolkit';
import allState from './states';

const parentFlowSlice=createSlice({
    name:'parentFlow',
    initialState:allState.localStates,
    reducers:{
        setIntroStatus:(state,action)=>{
            state.introStatus=action.payload.introStatus
        },
        
    }
})
export const { setIntroStatus } = parentFlowSlice.actions;

export default parentFlowSlice.reducer;