import { configureStore,combineReducers } from '@reduxjs/toolkit';
import counterReducer from '../redux/ParentFlow'; // Adjust the path based on your project structure
import AsyncStorage from '@react-native-async-storage/async-storage';
import { persistReducer, persistStore } from 'redux-persist';
import thunk from 'redux-thunk';


const config={
    key:"root",
    storage:AsyncStorage
} 
const rootReducer = combineReducers({
  counter: counterReducer,
});
const persistedReducer=persistReducer(config,rootReducer)
export const store=configureStore({
    reducer: {
        counter:persistedReducer ,
      },
      middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false, // Disable the serializable check for redux-persist
    }),
})

export const persistor=persistStore(store)