import Intro1 from '../assets/images/Intro1.png';
import Intro2 from '../assets/images/Intro2.png';
import Intro3 from '../assets/images/Intro3.png';
import BackIcon from '../assets/images/BackIcon.png';
import Phone from '../assets/images/Phone.png';
import userIcon from '../assets/images/userIcon.png';
import lockIcon from '../assets/images/lockIcon.png';
import menu from '../assets/images/menu.png';
import Logo from '../assets/images/Logo.png';
import bell from '../assets/images/bell.png';
import bell2 from '../assets/images/bell2.png';
import profil from '../assets/images/Profil.png';
import profil1 from '../assets/images/Profil1.png';
import profil2 from '../assets/images/Profil2.png';
import profil3 from '../assets/images/Profil3.png';
import phone1 from '../assets/images/phone1.png';
import Time from '../assets/images/Time.png';
import PetImage from '../assets/images/PetImage.png';
import Icon from '../assets/images/Icon.png';
import uploadImage from '../assets/images/uploadImage.png';
import DrawerImage from '../assets/images/DrawerImage.png';
import calendar from '../assets/images/calendar.png';
import homeIcon from '../assets/images/homeIcon.png';
import petfood from '../assets/images/petfood.png';
import note from '../assets/images/note.png';
import exclamation from '../assets/images/exclamation.png';
import logout from '../assets/images/logout.png';
import angleright from '../assets/images/angleright.png';
import aboutVet from '../assets/images/aboutVet.png';
import profile from '../assets/images/profile.png';
import privacy from '../assets/images/privacy.png';
import terms from '../assets/images/terms.png';
import logoutModal from '../assets/images/logoutModal.png';
import DogImg from '../assets/images/DogImg.png';
import prescription from '../assets/images/prescription.png';
import eye from '../assets/images/eye.png';
import flask from '../assets/images/flask.png';
import info from '../assets/images/info.png';
import vacination from '../assets/images/vacination.png';
import checkbutton from '../assets/images/checkbutton.png';
import uncheckbutton from '../assets/images/uncheckbutton.png';
import qrImage from '../assets/images/qrImage.png';
import screenshotImage from '../assets/images/screenshotImage.png';
import IDCard1 from '../assets/images/IDCard1.png';
import IDCard2 from '../assets/images/IDCard2.png';
import cross from '../assets/images/cross.png';
import checkbox from '../assets/images/checkbox.png';
import HomeLogo from '../assets/images/HomeLogo.png';
import Notifybell from '../assets/images/Notifybell.png';
import DashImage1 from '../assets/images/DashImage1.png';
import DashImage2 from '../assets/images/DashImage2.png';
import DashImage3 from '../assets/images/DashImage3.png';
import DashImage4 from '../assets/images/DashImage4.png';
import DashImage5 from '../assets/images/DashImage5.png';
import {moderateVerticalScale} from 'react-native-size-matters';
import {Height} from '../dimensions/dimensions';
import {Platform} from 'react-native';

export const imageConstants = {
  Intro1,
  Intro2,
  Intro3,
  BackIcon,
  Phone,
  userIcon,
  lockIcon,
  bell,
  menu,
  Logo,
  profil,
  profil1,
  profil2,
  profil3,
  phone1,
  Time,
  PetImage,
  Icon,
  uploadImage,
  DrawerImage,
  note,
  homeIcon,
  petfood,
  exclamation,
  calendar,
  logout,
  angleright,
  aboutVet,
  privacy,
  profile,
  terms,
  logoutModal,
  DogImg,
  prescription,
  eye,
  flask,
  info,
  vacination,
  checkbutton,
  uncheckbutton,
  qrImage,
  screenshotImage,
  IDCard1,
  IDCard2,
  cross,
  bell2,
  checkbox,
  HomeLogo,
  Notifybell,
  DashImage1,
  DashImage2,
  DashImage3,
  DashImage4,
  DashImage5,
};

export const fontConstant = {
  bold: 'Inter-Bold',
  extraBold: 'Inter-ExtraBold',
  light: 'Inter-Light',
  medium: 'Inter-Medium',
  regular: 'Inter-Regular',
  semiBold: 'Inter-SemiBold',
};

export const colorConstant = {
  white: '#FFFFFF',
  green: '#00A56A',
  yellow: '#FF8C00',
  red: '#ED1B24',
  orange: '#EC6602',
  black: '#111111',
  textColor: '#323232',
  buttonColor: '#134094',
  secondaryText: '#8F8F8F',
  inputBackground: '#F3F3F3',
  borderColor: '#0000004D',
  placeholderTextColor: '#8F8F8F',
};

export const shadowObject = {
  shadowColor: '#0000004D',
  shadowOffset: {
    width: 0,
    height: 1,
  },
  shadowOpacity: 0.22,
  shadowRadius: 2.22,

  elevation: 3,
};

export const HEADER_MARGIN_IOS =
  Height > 670 ? moderateVerticalScale(50) : moderateVerticalScale(40);

export const iphone8 = Height < 670 ? true : false;

export const MARGIN_BOTTOM =
  Platform.OS == 'ios' ? moderateVerticalScale(20) : moderateVerticalScale(40);
