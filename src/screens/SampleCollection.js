import {ScrollView, StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';

import {colorConstant, imageConstants} from '../utils/constants';
import CustomSampleInput from '../Custom/CustomSampleInput';
import {moderateVerticalScale} from 'react-native-size-matters';
import CustomButton from '../Custom/CustomButton';
import {Width} from '../dimensions/dimensions';
import CustomModal from '../Custom/CustomModal';
import CustomInfoModal from '../Custom/CustomInfoModal';
import CustomHeaderText from '../Custom/CustomHeaderText';

const SampleCollection = props => {
  const [isModalVisible, setModalVisible] = useState(false);
  const [isInfoModalVisible, setInfoModalVisible] = useState(false);
  const screenName = 'SampleCollection';
  const toggleModal = () => {
    console.log('logout working');
    setModalVisible(!isModalVisible);
  };

  const toggleInfoModal = () => {
    setInfoModalVisible(!isInfoModalVisible);
  };

  const ResetFunction = () => {
    setModalVisible(false);
  };
  return (
    <View style={styles.main}>
      <CustomHeaderText
        arrowIcon={imageConstants.BackIcon}
        headTitle={'Sample Collection'}
        onPress={() => props.navigation.goBack()}
      />

      <ScrollView
        contentContainerStyle={{
          paddingBottom: moderateVerticalScale(100),
        }}>
        <CustomSampleInput
          onPress={() => toggleModal()}
          infoModal={() => toggleInfoModal()}
          placeholder={'Type of Sample Collected'}
        />

        <CustomSampleInput
          onPress={() => toggleModal()}
          infoModal={() => toggleInfoModal()}
          placeholder={'Type of Sample Collected'}
        />

        <CustomSampleInput
          placeholder={'Type of Sample Collected'}
          reasonFlag={true}
        />
      </ScrollView>

      <View style={styles.BottomView}>
        <CustomButton
          OnButtonPress={() =>
            props.navigation.navigate('DetailScreen', {screenName})
          }
          buttonText={'Proceed'}
          color={colorConstant.white}
        />
      </View>

      <CustomModal
        isModalVisible={isModalVisible}
        setModalVisible={setModalVisible}
        OnButtonCancel={ResetFunction}
      />

      <CustomInfoModal
        contentTrue={true}
        isInfoModalVisible={isInfoModalVisible}
        setInfoModalVisible={setInfoModalVisible}
        onRequestClose={() => setInfoModalVisible(!isInfoModalVisible)}
      />
    </View>
  );
};

export default SampleCollection;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: '#EDF7FF',
  },

  BottomView: {
    position: 'absolute',
    bottom: 0,
    backgroundColor: colorConstant.white,
    height: moderateVerticalScale(90),
    width: Width,
  },
});
