import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstants } from '../utils/constants'

import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import CustomHeaderText from '../Custom/CustomHeaderText'


const Account = (props) => {
  return (
    <View style={styles.main}>
            <CustomHeaderText
                arrowIcon={imageConstants.BackIcon}
                headTitle={"My Account"}
                onPress={() => props.navigation.goBack()}
            />
            <View style={styles.container}>
            <TouchableOpacity style={styles.subContainer} onPress={() => props.navigation.navigate("Profile")}>
                <Image
                source={imageConstants.profile}
               resizeMode='contain'
                style={styles.ImageStyle}
                />
                <Text style={styles.TextStyle}>My Profile</Text>
                
                </TouchableOpacity>
                <View style={{ width: "100%", borderBottomWidth: 1, borderBottomColor: "#3232321A", marginBottom: moderateVerticalScale(4) }} />

                <TouchableOpacity style={styles.subContainer} onPress={() => props.navigation.navigate("About")}>
                <Image
                source={imageConstants.aboutVet}
               resizeMode='contain'
                style={styles.ImageStyle}
                />
                <Text style={styles.TextStyle}>About VETLAB</Text>
                
                </TouchableOpacity>

                <View style={{ width: "100%", borderBottomWidth: 1, borderBottomColor: "#3232321A", marginBottom: moderateVerticalScale(4) }} />

                <TouchableOpacity style={styles.subContainer} onPress={() => props.navigation.navigate("Privacy")}>
                <Image
                source={imageConstants.privacy}
               resizeMode='contain'
                style={styles.ImageStyle}
                />
                <Text style={styles.TextStyle}>Privacy Policy</Text>
                
                </TouchableOpacity>

                <View style={{ width: "100%", borderBottomWidth: 1, borderBottomColor: "#3232321A", marginBottom: moderateVerticalScale(4) }} />

                <TouchableOpacity style={styles.subContainer} onPress={() => props.navigation.navigate("Terms")}>
                <Image
                source={imageConstants.terms}
               resizeMode='contain'
                style={styles.ImageStyle}
                />
                <Text style={styles.TextStyle}>Terms & Conditions</Text>
                
                </TouchableOpacity>

                <View style={{ width: "100%", borderBottomWidth: 1, borderBottomColor: "#3232321A", marginBottom: moderateVerticalScale(4) }} />
               

            </View>
    </View>
  )
}

export default Account

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: "#EDF7FF"
    },

    container: {
        backgroundColor: colorConstant.white,
        width: "90%",
        borderColor: colorConstant.borderColor,
        borderWidth: 0.5,
       paddingHorizontal: moderateScale(20),
        marginTop: moderateVerticalScale(30),
        alignSelf: "center",
        borderRadius: 20,
        paddingVertical: moderateVerticalScale(10)
    },
    subContainer:{
        flexDirection:"row",
        alignItems:"center",
        paddingVertical: moderateVerticalScale(8),

    },
    ImageStyle:{
        width:50,
        height:50
    },
    TextStyle:{
        fontFamily: fontConstant.medium,
        fontSize:14,
        color: colorConstant.black,
        paddingHorizontal: moderateScale(15)
    }
})