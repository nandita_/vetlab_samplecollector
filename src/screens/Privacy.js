import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstants } from '../utils/constants'

import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import CustomHeaderText from '../Custom/CustomHeaderText'

const Privacy = (props) => {
  return (
    <View style={styles.main}>
    <CustomHeaderText
        arrowIcon={imageConstants.BackIcon}
        headTitle={"Privacy Policy"}
        onPress={() => props.navigation.goBack()}
    />

    <Text style={{
      paddingHorizontal: moderateScale(20),
      marginTop: moderateVerticalScale(30),
      fontFamily: fontConstant.medium,
      fontSize:16,
      color: colorConstant.black
    }}>
      Privacy Policy
    </Text>

    <Text style={{
      paddingHorizontal: moderateScale(20),
      marginTop: moderateVerticalScale(8),
      fontFamily: fontConstant.regular,
      fontSize:13,
      color: colorConstant.black
    }}>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</Text>
    </View>
  )
}

export default Privacy

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor: "#EDF7FF"
      }
})