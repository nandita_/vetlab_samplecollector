import { FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'

import { colorConstant, fontConstant, imageConstants } from '../utils/constants'
import CustomText from '../Custom/CustomText'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import CustomTotal from '../Custom/CustomTotal'
import { Width } from '../dimensions/dimensions'
import CustomButton from '../Custom/CustomButton'
import CustomHeaderText from '../Custom/CustomHeaderText'

const Appointment = (props) => {
   const screenName = "ScreenShotImage"
 const [active , setActive] = useState(false)
    const paymentdata = [
        {
            id:1,
            title:"UPI"
        },
        {
            id:2,
            title:"Show QR Code"
        },
        {
            id:3,
            title:"Cash"
        }
    ]

    // console.log(active , "dataaa");
    const handlePress = (index) => {
        setActive(index === active ? -1 : index);
      };

    const renderItem = ({item , index}) => {
        // console.log(index , "check index");

        return(
            <View style={styles.innerContainer} >
                <View style={{
                    flexDirection:"row",
                    alignItems:"center",
                    justifyContent:"space-between",
                    width:"90%",
                    alignSelf:"center",
                    // paddingBottom:10
                //    marginTop:10
                }}>
             <Text style={{
                fontFamily: fontConstant.medium,
                fontSize:14,
                color: colorConstant.black
             }}>{item.title}</Text>
             <TouchableOpacity onPress={() => handlePress(index)}>
             <Image 
             source={index === active ? imageConstants.checkbutton : imageConstants.uncheckbutton}
             resizeMode='contain'
             style={{
                width:25,
                height:25
             }}
             />
             </TouchableOpacity>
             </View>
             <View style={{width:"90%" , borderBottomColor:"#DBDBDB",  borderBottomWidth:1, alignSelf:"center" , paddingBottom:10 }}/>
            </View>
        )
    }
    return (
        <View style={styles.main}>
            <CustomHeaderText
                arrowIcon={imageConstants.BackIcon}
                headTitle={"Appointment Schedule"}
                // width={"60%"}
                onPress={() => props.navigation.goBack()}
            />

            <CustomText
                Title={"Order Summary"}
                paddingHorizontal={ moderateScale(10)}
                fontFamily={fontConstant.bold}
            />

            <CustomTotal 
            title1={"Consultation Fee"}
            title2={"Discounted Price"}
            title3={"Collection fee (incl. Consumables & Transportation Fees)"}
            title4={"Amount Deducted (Sample not collected)"}
            />

            <CustomText 
            Title={"Select payment method"}
            paddingHorizontal={ moderateScale(10)}
            fontFamily={fontConstant.bold}
            marginTop={moderateVerticalScale(20)}
            />

            <View style={styles.paymentContainer}>
             <FlatList 
             data={paymentdata}
             renderItem={renderItem}
             />
            </View>

            <View style={styles.BottomView}>
                       <CustomButton
                       OnButtonPress={() => {
                        if(active === 0) {
                            props.navigation.navigate("ScreenShotImage")
                        }
                        if(active === 1) {
                            props.navigation.navigate("QRscreen")
                        }
                        if(active === 2) {
                            props.navigation.navigate("DetailScreen" , {screenName})
                        }
                        }}
                buttonText={active === 0 ? "Capture Screenshot" : active === 1 ? "Show QR Code" : active === 2 ? "Collect Cash" : "Proceed"}
                color={colorConstant.white}
                />
                    </View>

        </View>
    )
}

export default Appointment

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: "#EDF7FF"
    },

    paymentContainer:{
        width: "90%",
        backgroundColor: colorConstant.white,
        // paddingHorizontal:"5%",
        marginTop: moderateVerticalScale(20),
        paddingBottom: moderateVerticalScale(20),
        borderRadius: 18,
        borderColor: colorConstant.borderColor,
        borderWidth: 0.8,
        // height:"50%",
        alignSelf: "center",
    },

    innerContainer:{
        padding: moderateVerticalScale(8),
        marginTop:moderateVerticalScale(8),
        
    },
    BottomView: {
        position: "absolute",
        bottom: 0,
        backgroundColor: colorConstant.white,
        height: moderateVerticalScale(90),
        width: Width
    },
})