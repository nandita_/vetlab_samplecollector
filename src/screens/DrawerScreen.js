import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import {colorConstant, fontConstant, imageConstants} from '../utils/constants';
import {moderateScale, moderateVerticalScale} from 'react-native-size-matters';
import Dashboard from './Dashboard';
import {useNavigation} from '@react-navigation/native';
import CustomLogoutModal from '../Custom/CustomLogoutModal';
import {Width} from '../dimensions/dimensions';
import {useDispatch, useSelector} from 'react-redux';
import {setIntroStatus} from '../redux/ParentFlow';
import {store} from '../utils/store';

const DrawerScreen = () => {
  const [isModalVisible, setModalVisible] = useState(false);
  const introStatus = useSelector(state => state.counter.counter.introStatus);
  // console.log(introStatus , "INNDJJ");
  const dispatch = useDispatch();
  const toggleModal = () => {
    console.log('logout working');
    setModalVisible(!isModalVisible);
  };

  const ResetFunction = () => {
    setModalVisible(false);
  };
  return (
    <View style={styles.main}>
      <View style={styles.headContainer}>
        <Image
          source={imageConstants.DrawerImage}
          resizeMode="contain"
          style={{
            width: 60,
            height: 60,
            marginHorizontal: moderateScale(30),
            marginTop: moderateVerticalScale(10),
          }}
        />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            width: '90%',
            left: '6%',
            //  paddingLeft:"5%",
            // alignSelf: "center",
            // backgroundColor:"pink",
            marginTop: moderateVerticalScale(10),
          }}>
          <View>
            <Text style={styles.nameText}>Leslie Alexander</Text>
            <Text style={styles.sectorText}>Location :- sector 70, NOIDA</Text>
          </View>
          {/* <Image source={imageConstants.angleright}
            resizeMode='contain'
            style={{
              width: 20,
              // left:"50%",
              height: 20
            }}
          /> */}
        </View>

        <View
          style={{
            width: '90%',
            height: 0.8,
            marginTop: moderateVerticalScale(20),
            alignSelf: 'center',
            backgroundColor: colorConstant.white,
            borderRadius: 100,
            left: '5%',
          }}
        />

        <ButtonName
          image={imageConstants.homeIcon}
          // image1={imageConstants.arrow_right}
          name={'Home'}
          pathName={'Home'}
        />

        <ButtonName
          image={imageConstants.calendar}
          // image1={imageConstants.arrow_right}
          name={'Dashboard'}
          pathName={'Dashboard'}
        />

        <ButtonName
          image={imageConstants.petfood}
          // image1={imageConstants.arrow_right}
          name={'Upcoming Sample Collection'}
          pathName={'PendingHomeCollection'}
        />

        <ButtonName
          image={imageConstants.note}
          // image1={imageConstants.arrow_right}
          name={'Sample Collection History'}
          pathName={'HomeCollectionHistory'}
          // tabIndex={1}
        />

        <ButtonName
          image={imageConstants.exclamation}
          // image1={imageConstants.arrow_right}
          name={'My Account'}
          pathName={'Account'}
        />
      </View>

      <TouchableOpacity
        style={{
          padding: moderateScale(10),
          alignItems: 'center',
          flexDirection: 'row',
          // paddingHorizontal: moderateScale(20),
          position: 'absolute',
          width: '90%',
          alignSelf: 'center',
          bottom: moderateVerticalScale(20),
        }}
        activeOpacity={0.9}>
        <View style={styles.ImageView}>
          <Image
            style={styles.img}
            resizeMode="contain"
            source={imageConstants.logout}
          />
        </View>
        <TouchableOpacity onPress={() => toggleModal()}>
          <Text
            style={[
              styles.text1,
              {
                width: '100%',
              },
            ]}>
            Logout
          </Text>
        </TouchableOpacity>

        <CustomLogoutModal
          onRequestClose={() => setModalVisible(!isModalVisible)}
          // bottom={30}
          buttonText={'Logout'}
          cancelText={'Cancel'}
          headerText={'Do you want to Logout'}
          // subText={translations.sure_logout}
          closeButtonTitle="Okay"
          // buttonText={translations.Yes_Delete}
          isModalVisible={isModalVisible}
          setModalVisible={setModalVisible}
          OnButtonCancel={ResetFunction}
          OnButtonDelete={() =>
            // props.navigation.navigate("Login")
            store.dispatch(setIntroStatus({introStatus: 'auth'}))
          }
        />
      </TouchableOpacity>

      <View style={styles.shadowView} />
    </View>
  );
};

export default DrawerScreen;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.buttonColor,
  },

  headContainer: {
    // paddingHorizontal: moderateScale(20),
    ...Platform.select({
      ios: {
        marginTop: HEADER_MARGIN_IOS,
      },
      android: {
        marginTop: moderateVerticalScale(30),
      },
    }),
  },
  button: {
    flexDirection: 'row',
    // backgroundColor: colorConstant.homebackground + '20',
    width: '90%',
    alignSelf: 'center',
    // padding: moderateScale(10),
    paddingHorizontal: moderateScale(10),
    // paddingVertical:moderateVerticalScale(10),
    alignItems: 'center',
    marginTop: moderateVerticalScale(20),
    // borderWidth:2,
  },
  img: {
    width: 22,
    height: 22,
  },
  ImageView: {
    backgroundColor: colorConstant.white + 30,
    padding: moderateScale(15),
    //  paddingVertical: moderateVerticalScale(10),
    borderRadius: 10,
  },

  text1: {
    fontSize: 14,
    lineHeight: 25,
    width: '55%',
    fontFamily: fontConstant.medium,
    color: colorConstant.white,
    // lineHeight: 28,
    marginLeft: 15,
  },

  nameText: {
    fontFamily: fontConstant.bold,
    fontSize: 16,
    color: colorConstant.white,
    lineHeight: 25,
  },
  sectorText: {
    fontFamily: fontConstant.medium,
    fontSize: 14,
    color: colorConstant.white,
    lineHeight: 25,
  },

  //   shadowView:{
  // position: 'absolute',
  // backgroundColor: 'red',
  // height: 200,
  // width: 50,

  //   },
});

const ButtonName = ({
  name,
  pathName,
  image,
  // tabIndex,
  image1,
  screenName,
  isAuthorised,
  apiData1,
}) => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      style={styles.button}
      activeOpacity={0.9}
      onPress={() => {
        if (name == 'Logout') {
          toggleModal();
        } else if (pathName) {
          navigation.navigate(pathName);
        } else {
          console.log('erro');
        }
      }}>
      <View style={styles.ImageView}>
        <Image style={styles.img} resizeMode="contain" source={image} />
      </View>
      {name == 'Logout' ? (
        <Text
          style={[
            styles.text1,
            {
              color: 'red',
            },
          ]}>
          {name}
        </Text>
      ) : (
        <Text style={styles.text1}>{name || ''}</Text>
      )}
    </TouchableOpacity>
  );
};
