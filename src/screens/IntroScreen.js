import {
  FlatList,
  Image,
  ImageBackground,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useCallback, useRef, useState} from 'react';
import {
  moderateScale,
  moderateVerticalScale,
  scale,
} from 'react-native-size-matters';

import {Height, Width} from '../dimensions/dimensions';
import {colorConstant, fontConstant, imageConstants} from '../utils/constants';
import CustomButton from '../Custom/CustomButton';
import {useDispatch, useSelector} from 'react-redux';

import {setIntroStatus} from '../redux/ParentFlow';
import {store} from '../utils/store';

let indexx = Number(0);

const IntroScreen = props => {
  const [currentPage, setCurrentPage] = useState(0);
  // const introStatus=useSelector((state)=>state.counter.counter.introStatus)
  const dispatch = useDispatch();

  // console.log("reduxwala====>",introStatus)
  let flatListRef = useRef();
  const scrollToIndex = index => {
    flatListRef.current.scrollToIndex({index});
    setCurrentPage(index);
    indexx = parseInt(index);
    console.log('asdkas=>', currentPage, typeof indexx);
  };

  // const handleNextPress = () => {
  //     // Call the Swiper's scrollBy method to move to the next slide
  //     flatListRef.current.scrollBy(1);
  //   };

  const _onViewableItemsChanged = useCallback(({viewableItems, changed}) => {
    setCurrentPage(viewableItems?.[0]?.key);
    indexx = parseInt(viewableItems?.[0]?.key);
    console.log('qwert=>', currentPage, typeof indexx);
  }, []);
  const _viewabilityConfig = {
    // waitForInteraction: true,
    // viewAreaCoveragePercentThreshold: 95,
    itemVisiblePercentThreshold: 50,
  };
  const handlePress = () => {
    console.log('===>huihui', currentPage, typeof indexx);
    // if(currentPage == 0){
    //   indexx = 1

    // }else if(currentPage == 1){
    //   setCurrentPage(2)
    // }
    indexx += Number(1);

    // setCurrentPage(indexx)
    // console.log
    console.log('===>huihuiashjdajsv', currentPage, typeof indexx);

    flatListRef.current.scrollToIndex({
      animated: true,
      index: indexx,
      viewPosition: 0.5,
    });
  };

  const OnboardingData = [
    {screen: <Screen1 scrollToIndex={scrollToIndex} props={props} />},
    {screen: <Screen2 scrollToIndex={scrollToIndex} props={props} />},
    {screen: <Screen3 />},
  ];

  const cardTittle = {
    0: `Manage All Your Pet \n Records`,
    1: `Home Collection And \n Centre Visit Appointments`,
    2: 'Video Consultations',
  };
  const cardDescription = {
    0: `You can keep and manage Health records of all your pets easily on VETLAB EZiconnect along \n with due date reminders of vaccination and \n deworming.`,
    1: `You can easily book appointment on VETLAB EZiconnect just with on one click for home sample collection or visit to any of our centre `,
    2: `You can easily book video consultations on VETLAB EZiconnect with best and experienced vet professionals as per your convenience. `,
  };

  const renderItem = ({item}) => {
    // console.log(item);
    return <View style={{width: Width, height: Height}}>{item?.screen}</View>;
  };

  return (
    <>
      <StatusBar
        barStyle={'dark-content'}
        translucent={true}
        // backgroundColor={props.bgColor?props.bgColor:"white"}
        backgroundColor={'transparent'}
      />
      <View style={{flex: 1, backgroundColor: '#fffff'}}>
        <FlatList
          pagingEnabled
          ref={flatListRef}
          extraData={currentPage}
          horizontal
          data={OnboardingData}
          renderItem={renderItem}
          onViewableItemsChanged={_onViewableItemsChanged}
          viewabilityConfig={_viewabilityConfig}
          // onScroll={handleScroll}
        />
        <View
          style={{
            height: '48%',
            backgroundColor: '#ffffff',
            marginTop: -30,
            justifyContent: 'space-between',
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
          }}>
          <View
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: 20,
            }}>
            <Image
              source={imageConstants.Logo}
              resizeMode="contain"
              style={{
                width: Width * 0.4,
                height: Height * 0.08,
                alignSelf: 'center',
              }}
            />
            <View
              style={{
                minHeight: moderateVerticalScale(180),
              }}>
              <Text
                style={{
                  minHeight: moderateVerticalScale(70),
                  width: '95%',
                  alignSelf: 'center',
                  color: '#323232',
                  fontFamily: fontConstant.bold,
                  fontSize: 25,
                  lineHeight: 30,
                  textAlign: 'center',
                  marginTop: moderateVerticalScale(15),
                }}>
                {cardTittle[currentPage]}
              </Text>

              <Text
                style={{
                  minHeight: moderateVerticalScale(56),
                  width: '95%',
                  alignSelf: 'center',
                  color: '#323232',
                  lineHeight: 22,
                  fontFamily: fontConstant.regular,
                  fontSize: 14.5,
                  textAlign: 'center',
                  marginTop: 10,
                }}>
                {cardDescription[currentPage]}
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
                marginTop: moderateVerticalScale(10),
              }}>
              {OnboardingData?.map((_, index) => (
                <View
                  key={index}
                  style={[
                    styles.paginationDot,
                    {
                      backgroundColor:
                        index === indexx ? '#4F4F4F' : 'rgba(79, 79, 79, 0.20)',
                      width: index === indexx ? 25 : 10,
                    },
                  ]}
                />
              ))}
            </View>
          </View>

          {currentPage == 2 ? (
            <CustomButton
              OnButtonPress={() => {
                // props.navigation.navigate("Login")
                store.dispatch(setIntroStatus({introStatus: 'auth'}));
              }}
              buttonText={'Get Started'}
              color={colorConstant.white}
              bottom={moderateVerticalScale(15)}
            />
          ) : (
            <CustomButton
              buttonText={'Next'}
              OnButtonPress={() => {
                handlePress();
              }}
              color={colorConstant.white}
              bottom={moderateVerticalScale(15)}
            />
          )}
        </View>
      </View>
    </>
  );
};

export default IntroScreen;

const styles = StyleSheet.create({
  paginationDot: {
    height: 10,
    width: 10,
    marginRight: 10,
    borderRadius: 100,
  },
});

const Screen1 = ({props}) => {
  const dispatch = useDispatch();
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#EBD6CB',
      }}>
      <Text
        onPress={() => {
          // props.navigation.navigate("Login")
          store.dispatch(setIntroStatus({introStatus: 'auth'}));
        }}
        style={{
          color: colorConstant.white,
          position: 'absolute',
          zIndex: 999,
          top: '8%',
          right: 20,
          fontFamily: fontConstant.semiBold,
          fontSize: 14,
          // lineHeight: 17,
        }}>
        Skip
      </Text>
      <Image
        style={{
          width: Width,
          height: Height * 0.75,
          // backgroundColor: 'black',
        }}
        source={imageConstants.Intro1}
        resizeMode="cover"
      />
    </View>
  );
};
const Screen2 = ({props}) => {
  const dispatch = useDispatch();
  return (
    <View style={{flex: 1}}>
      <Text
        onPress={() => {
          // props.navigation.navigate("Login")
          store.dispatch(setIntroStatus({introStatus: 'auth'}));
        }}
        style={{
          color: colorConstant.white,
          position: 'absolute',
          zIndex: 999,
          top: '8%',
          right: 20,
          fontFamily: fontConstant.semiBold,
          fontSize: 14,
          // lineHeight: 17,
        }}>
        Skip
      </Text>
      <Image
        style={{width: Width, height: Height * 0.75}}
        source={imageConstants.Intro2}
        resizeMode="cover"
      />
    </View>
  );
};
const Screen3 = () => {
  return (
    <View style={{flex: 1, backgroundColor: '#e2a8b1'}}>
      <Image
        style={{width: Width, height: Height * 0.75}}
        source={imageConstants.Intro3}
        resizeMode="cover"
      />
    </View>
  );
};
