import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'

import { colorConstant, fontConstant, imageConstants } from '../utils/constants'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import CustomInfoModal from '../Custom/CustomInfoModal'
import CustomHeaderText from '../Custom/CustomHeaderText'

const Profile = (props) => {
    const [isInfoModalVisible, setInfoModalVisible] = useState(false);
  const toggleModal = () => {
    console.log('logout working');
    setInfoModalVisible(!isInfoModalVisible);
  };

    return (
        <View style={styles.main}>
            <CustomHeaderText
                arrowIcon={imageConstants.BackIcon}
                headTitle={"Profile"}
                onPress={() => props.navigation.goBack()}
            />
            <Image source={imageConstants.profil}
                resizeMode='contain'
                style={{
                    width: 60,
                    height: 60,
                    borderRadius: 50,
                    position: "absolute",
                    alignSelf: "center",
                    top: "18%",
                    zIndex: 100
                    // backgroundColor:"pink "
                }}
            />

            <View style={styles.container}>
                <View style={styles.TextContainer}>
                    <Text style={styles.headTitle}>Full name</Text>
                    <Text style={styles.subTitle}>Leslie Alexander</Text>
                </View>

                <View style={{ borderBottomWidth: 1, borderBottomColor: "#3232321A", width: "90%", alignSelf: "center", marginTop: moderateVerticalScale(15) }} />
                <View style={[styles.TextContainer, {
                    marginTop: moderateVerticalScale(20)
                }]}>
                    <Text style={styles.headTitle}>Age</Text>
                    <Text style={styles.subTitle}>48</Text>
                </View>

                <View style={{ borderBottomWidth: 1, borderBottomColor: "#3232321A", width: "90%", alignSelf: "center", marginTop: moderateVerticalScale(20) }} />

                <View style={[styles.TextContainer, {
                    marginTop: moderateVerticalScale(20)
                }]}>
                    <Text style={styles.headTitle}>Contact Number</Text>
                    <Text style={styles.subTitle}>+911234567890</Text>
                </View>

                <View style={{ borderBottomWidth: 1, borderBottomColor: "#3232321A", width: "90%", alignSelf: "center", marginTop: moderateVerticalScale(20) }} />

                <View style={[styles.TextContainer, {
                    marginTop: moderateVerticalScale(20)
                }]}>
                    <Text style={styles.headTitle}>Highest Qualification</Text>
                    <Text style={styles.subTitle}>Master</Text>
                </View>

                <View style={{ borderBottomWidth: 1, borderBottomColor: "#3232321A", width: "90%", alignSelf: "center", marginTop: moderateVerticalScale(20) }} />

                <View style={[styles.TextContainer, {
                    marginTop: moderateVerticalScale(20)
                }]}>
                    <Text style={styles.headTitle}>Experience</Text>
                    <Text style={styles.subTitle}>2 Years</Text>
                </View>

                <View style={{ borderBottomWidth: 1, borderBottomColor: "#3232321A", width: "90%", alignSelf: "center", marginTop: moderateVerticalScale(20) }} />

                <View style={[styles.TextContainer, {
                    marginTop: moderateVerticalScale(20)
                }]}>
                    <Text style={styles.headTitle}>Assigned Location</Text>
                    <Text style={styles.subTitle}>Noida sector 63</Text>
                </View>

                <View style={{ borderBottomWidth: 1, borderBottomColor: "#3232321A", width: "90%", alignSelf: "center", marginTop: moderateVerticalScale(20) }} />

                <View style={[styles.TextContainer, {
                    marginTop: moderateVerticalScale(20)
                }]}>
                    <Text style={styles.headTitle}>Gender</Text>
                    <Text style={styles.subTitle}>Male</Text>
                </View>

                <View style={{ borderBottomWidth: 1, borderBottomColor: "#3232321A", width: "90%", alignSelf: "center", marginTop: moderateVerticalScale(20) }} />

                <View style={[styles.TextContainer1, {
                    marginTop: moderateVerticalScale(20),
                }]}>
                    <Text style={styles.headTitle}>Emp. ID</Text>
                    <Text style={styles.subTitle}>#VL0123</Text>
                   
                </View>

                <View style={{ borderBottomWidth: 1, borderBottomColor: "#3232321A", width: "90%", alignSelf: "center", marginTop: moderateVerticalScale(20) }} />
 
                <View style={[styles.TextContainer1, {
                    marginTop: moderateVerticalScale(20),
                }]}>
                    <Text style={styles.headTitle}>View ID Card</Text>
                    {/* <Text style={styles.subTitle}>#VL0123</Text> */}
                    <TouchableOpacity 
                    onPress={() =>toggleModal() }
                    style={{
                        position: "absolute",
                        right: 10
                    }}>
                        <Image
                            source={imageConstants.eye}
                            resizeMode='contain'
                            style={{
                                width: 20,
                                height: 20,


                            }}
                        />
                    </TouchableOpacity>
                </View>

                <View style={{ borderBottomWidth: 1, borderBottomColor: "#3232321A", width: "90%", alignSelf: "center", marginTop: moderateVerticalScale(20) }} />


            </View>


          <CustomInfoModal 
          setInfoModalVisible={setInfoModalVisible}
          isInfoModalVisible={isInfoModalVisible}
          />
            {/* <TouchableOpacity style={styles.DocContainer} activeOpacity={0.8} onPress={() => props.navigation.navigate("Document")}>
             <Image source={imageConstants.PetImage}
             resizeMode='contain'
             style={{
              width:50,
              height:50
             }} 
             
             />
             <View style={{
                paddingHorizontal: moderateScale(10),

             }}>
             <Text style={styles.DocText}>Documents</Text>
             <Text style={styles.CardText}>ID card</Text>
             </View>
            </TouchableOpacity> */}
        </View>
    )
}

export default Profile

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: "#EDF7FF"
    },
    container: {
        backgroundColor: colorConstant.white,
        width: "90%",
        borderColor: colorConstant.borderColor,
        borderWidth: 0.5,
        padding: moderateVerticalScale(10),
        marginTop: moderateVerticalScale(60),
        alignSelf: "center",
        borderRadius: 20,
        paddingVertical: moderateVerticalScale(10)
    },
    TextContainer: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        width: "90%",
        alignSelf: "center",
        marginTop: moderateVerticalScale(25)
    },
    TextContainer1: {
        flexDirection: "row",
        alignItems: "center",
        // backgroundColor:"pink",
        justifyContent: "space-between",
        width: "90%",
        alignSelf: "center",
        marginTop: moderateVerticalScale(30)
    },
    headTitle: {
        fontFamily: fontConstant.bold,
        fontSize: 14,
        color: colorConstant.black
    },
    subTitle: {
        fontFamily: fontConstant.medium,
        fontSize: 14,
        color: colorConstant.black
    },
    DocContainer: {
        backgroundColor: colorConstant.white,
        width: "90%",
        padding: moderateVerticalScale(10),
        marginTop: moderateVerticalScale(20),
        alignSelf: "center",
        borderRadius: 20,
        borderColor: colorConstant.borderColor,
        borderWidth: 0.5,
        paddingVertical: moderateVerticalScale(8),
        paddingHorizontal: moderateScale(12),
        flexDirection: "row",
        alignItems: "center"
    },
    DocText: {
        fontFamily: fontConstant.bold,
        fontSize: 14,
        lineHeight: 25,
        color: colorConstant.black
    },
    CardText: {
        fontFamily: fontConstant.medium,
        fontSize: 14,
        lineHeight: 28,
        color: colorConstant.black
    }
})