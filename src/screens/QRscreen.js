import { Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'

import { colorConstant, fontConstant, imageConstants } from '../utils/constants'
import { Height, Width } from '../dimensions/dimensions'
import { moderateVerticalScale } from 'react-native-size-matters'
import CustomHeaderText from '../Custom/CustomHeaderText'

const QRscreen = (props) => {
  return (
    <View style={styles.main}>
    <CustomHeaderText
        arrowIcon={imageConstants.BackIcon}
        headTitle={"QR Code Scan"}
        // width={"60%"}
        onPress={() => props.navigation.goBack()}
    />
   
   <View style={{
    position:"absolute",
    top:"25%",
    alignSelf:"center"
   }}>
    <Text style={{
        textAlign:"center",
        fontFamily: fontConstant.medium,
        fontSize:14,
        color: colorConstant.black
    }}>Scan to complete payment</Text>
    <Image 
    source={imageConstants.qrImage}
    resizeMode='contain'
    style={{
        width:Width*0.70,
        height:Height*0.45,
       
       
        
    }}
    />
    </View>
    </View>
  )
}

export default QRscreen

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: "#EDF7FF"
    },
})