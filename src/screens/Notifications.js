import { FlatList, StyleSheet, Text, View,Image } from 'react-native'
import React from 'react'

import { colorConstant, fontConstant, imageConstants } from '../utils/constants'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import CustomHeaderText from '../Custom/CustomHeaderText'
import { Width } from '../dimensions/dimensions'


const Notifications = (props) => {

    const Data = [
        {
        id : 1 , 
        image:imageConstants.Notifybell,
        Title:"Vaccination",
        desc:"Lorem ipsum dolor sit amet, consectetur",
        time:"05:00 PM"
    },
    {
        id: 2, 
        image:imageConstants.Notifybell,
        Title:"Video Constulant",
        desc:"Lorem ipsum dolor sit amet, consectetur",
        time:"05:00 PM"
    },
    {
        id: 3,
        image:imageConstants.bell2,
        Title:"Video Constulant",
        desc:"Lorem ipsum dolor sit amet, consectetur",
        time:"05:00 PM"
    },
    {
        id: 4,
        image:imageConstants.bell2,
        Title:"Vaccination",
        desc:"Lorem ipsum dolor sit amet, consectetur",
        time:"05:00 PM"
    }
]

const renderItem = ({item ,index}) => {
    return(
        <View style={styles.Listcontainer}>
        <View style={{flexDirection:"row", alignItems:"center"}}>
        <Image source={item.image} resizeMode='contain' style={{width:24, height:24}}/>
            <View style={{paddingLeft:"8%"}}>
         <Text style={styles.TitleText}>{item.Title}</Text>
         <Text style={styles.descText}>{item.desc}</Text>
         </View>
         </View>
         <Text style={styles.timeText}>{item.time}</Text>
         
         
        </View>
    )
}
  return (
    <View style={styles.main}>
    <CustomHeaderText
        arrowIcon={imageConstants.BackIcon}
        headTitle={"Notification"}
        onPress={() => props.navigation.goBack()}
    />

    <FlatList 
    data={Data}
    renderItem={renderItem}
    keyExtractor={item => item.id}
    />
    </View>
  )
}

export default Notifications

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: "#EDF7FF"
    },
    Listcontainer:{
        backgroundColor: colorConstant.white,
        alignSelf:"center",
        padding:"3%",
        width:"90%",
        borderRadius:15,
        borderWidth:0.8,
        borderColor:"#0000004D",
        paddingHorizontal: moderateScale(15),
        marginTop: moderateVerticalScale(18),
    },
    TitleText:{
        fontFamily: fontConstant.medium,
        fontSize:14,
        color: colorConstant.textColor
    },
    descText:{
        fontFamily: fontConstant.regular,
        fontSize:12,
        width:Width*0.70,
        color: colorConstant.secondaryText,
        lineHeight:20,
        marginTop:moderateVerticalScale(5)
    },
    timeText:{
        fontFamily: fontConstant.regular,
        color: colorConstant.secondaryText,
        fontSize:12,
        textAlign:"right",
        marginTop: moderateVerticalScale(3),
        // paddingVertical:moderateVerticalScale(10)
    }
})
