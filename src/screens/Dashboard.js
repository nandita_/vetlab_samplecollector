import { FlatList, Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'

import { colorConstant, fontConstant, imageConstants } from '../utils/constants'
import LinearGradient from 'react-native-linear-gradient'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import CustomHeaderText from '../Custom/CustomHeaderText'

const Dashboard = (props) => {
    const DATA = [
        {
          id: 1,
          title: 'Total Number of Home Collection',
          image: imageConstants.DashImage1,
          gradientColours: '["#37A7FE", "#134094"]',
          amount:"1500"
        },
        {
          id: 2,
          title: 'Total Completed',
          image: imageConstants.DashImage2,
          gradientColours: '["#16C687", "#00A66A"]',
          amount:"1500"
        },
        {
          id: 3,
          title: 'Total Cancelled',
          image: imageConstants.DashImage3,
          gradientColours: '["#FE3737", "#941313"]',
          amount:"1500"
        },
        {
            id: 4,
            title: 'Total Pending',
            image: imageConstants.DashImage4,
            gradientColours: '["#EC6602", "#FF8C00"]',
            amount:"1500"
          },
          {
            id: 5,
            title: 'Total Collection of the Month',
            image: imageConstants.DashImage5,
            gradientColours: '["#BE37FE", "#821394"]',
            amount:"₹ 1500"
          },
          {
            id: 6,
            title: 'Total Cash Collection of the Month',
            image: imageConstants.DashImage5,
            gradientColours: '["#3757FE", "#251394"]',
            amount:"₹ 1500"
          },
          
      ];

      const CustomBox = ({ title, gradientColours, amount , image}) => (

        <View style={{marginBottom: 20, }}>
        <LinearGradient colors={JSON.parse(gradientColours)}
        style={{ width: '95%', borderRadius: 20 ,alignSelf:"center", padding: "4%"}}
        >
          <View style={{
            flexDirection:"row",
            alignItems:"center"
          }}>
            <View style={{
              backgroundColor: colorConstant.white+50,
              padding: "4%",
              borderRadius:15
            }}>
          <Image 
          source={image}
          resizeMode='contain'
          style={{
            width:30,
            height:30,
            
          }}
          />
          </View>
          <View style={{
            paddingHorizontal: moderateVerticalScale(10),
            
          }}>
          <Text style={{
            fontFamily: fontConstant.medium,
            fontSize:14,
            lineHeight:30,
            color: colorConstant.white
          }}>
            {title}
          </Text>
          <Text style={{
            fontFamily: fontConstant.medium,
            fontSize:14,
            lineHeight:30,
            color: colorConstant.white
          }}>
            {amount}
            </Text>
          </View>
          </View>
        </LinearGradient>
        
        </View>
         );

      const renderItem = ({ item }) => <CustomBox 
      title={item.title} 
      gradientColours={item.gradientColours}
      amount={item.amount}
      image={item.image}
      />;
  
  return (
    <View style={styles.main}>
    <CustomHeaderText
        arrowIcon={imageConstants.BackIcon}
        headTitle={"Dashboard"}
        onPress={() => props.navigation.goBack()}
    />

    <View style={{
      marginTop: moderateVerticalScale(30)
    }}>
        <FlatList 
        data={DATA} 
        scrollEnabled={true}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        contentContainerStyle={{
          paddingBottom: moderateVerticalScale(100)
        }}
        />
    </View>
    </View>
  )
}

export default Dashboard

const styles = StyleSheet.create({
    main:{
        flex:1
    }
})