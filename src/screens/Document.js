import { Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import { colorConstant, fontConstant, imageConstants } from '../utils/constants'
import CustomText from '../Custom/CustomText'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import CustomInput from '../Custom/CustomInput'
import { Height, Width } from '../dimensions/dimensions'
import CustomButton from '../Custom/CustomButton'
import CustomHeaderText from '../Custom/CustomHeaderText'

const Document = (props) => {
    const [imagetrue, setImageTrue] = useState(false)
    return (
        <View style={styles.main}>
            <CustomHeaderText
                arrowIcon={imageConstants.BackIcon}
                headTitle={"Documents"}
                onPress={() => props.navigation.goBack()}
            />

            <CustomText
                Title={"ID Card"}
            />

            <View style={styles.container}>
                <CustomText
                    marginTop={moderateVerticalScale(1)}

                    Title={"Type of ID card"}
                />
                <TextInput
                    style={styles.InputField}
                    placeholder='Type of ID card'
                    placeholderTextColor={colorConstant.placeholderTextColor}
                />

                <CustomText
                    marginTop={moderateVerticalScale(15)}

                    Title={"Id Card Number"}
                />
                <TextInput
                    style={styles.InputField}
                    placeholder='Id Card Number'
                    placeholderTextColor={colorConstant.placeholderTextColor}
                />

                <CustomText
                    marginTop={moderateVerticalScale(15)}

                    Title={"Upload  ID Card"}
                />


                {
                    imagetrue ?
                        <View style={{
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "space-evenly",
                            width: "100%",
                            // backgroundColor:"pink"
                        }}>

                            <Image source={imageConstants.uploadImage}
                                resizeMode='contain'
                                style={{
                                    width: Width * 0.40,
                                    height: moderateVerticalScale(70),
                                    //    backgroundColor:"yellow",
                                    marginTop: moderateVerticalScale(20),
                                }}

                            />


                            <TouchableOpacity style={styles.DocContainer2} activeOpacity={0.8} onPress={() => setImageTrue(!imagetrue)} >

                                <Image source={imageConstants.Icon}
                                    resizeMode='contain'
                                    style={{
                                        width: 20,
                                        height: 20,
                                        alignSelf: "center"
                                    }}

                                />

                                <Text style={styles.DocText}>Upload Image</Text>
                            </TouchableOpacity>
                        </View>
                        :
                        <TouchableOpacity style={styles.DocContainer} activeOpacity={0.8} onPress={() => setImageTrue(!imagetrue)} >

                            <Image source={imageConstants.Icon}
                                resizeMode='contain'
                                style={{
                                    width: 20,
                                    height: 20,
                                    alignSelf: "center"
                                }}

                            />

                            <Text style={styles.DocText}>Upload Image</Text>
                        </TouchableOpacity>

                }





            </View>
            {
                !imagetrue && (
                    <View style={styles.BottomView}>
                       <CustomButton
                buttonText={"Save"}
                color={colorConstant.white}
                />
                    </View>
                )
            }

        </View>
    )
}

export default Document

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: "#EDF7FF"
    },

    container: {
        backgroundColor: colorConstant.white,
        width: "90%",
        borderColor: colorConstant.borderColor,
        borderWidth: 0.5,
        padding: moderateVerticalScale(0),
        marginTop: moderateVerticalScale(20),
        alignSelf: "center",
        borderRadius: 20,
        paddingVertical: moderateVerticalScale(20)
    },

    InputField: {
        padding: moderateScale(10),
        backgroundColor: colorConstant.white,
        borderWidth: 0.8,
        borderRadius: 15,
        borderColor: colorConstant.borderColor,
        width: "92%",
        marginTop: moderateVerticalScale(10),
        alignSelf: "center"
    },
    DocContainer: {
        backgroundColor: "#EDF7FF",
        width: "90%",
        height: moderateVerticalScale(70),
        marginTop: moderateVerticalScale(20),
        alignSelf: "center",
        justifyContent: "center",
        borderRadius: 10,
        borderColor: "#0174CC",
        borderWidth: 0.5,
        flexDirection: "row",
        alignItems: "center"
    },
    DocContainer2: {
        backgroundColor: "#EDF7FF",
        width: "50%",
        height: moderateVerticalScale(70),
        marginTop: moderateVerticalScale(20),
        alignSelf: "center",
        justifyContent: "center",
        borderRadius: 10,
        borderColor: "#0174CC",
        borderWidth: 0.5,
        flexDirection: "row",
        alignItems: "center"
    },
    DocText: {
        fontFamily: fontConstant.bold,
        fontSize: 14,
        paddingHorizontal: moderateScale(10),
        color: colorConstant.black
    },

    BottomView: {
        position: "absolute",
        bottom: 0,
        backgroundColor: colorConstant.white,
        height: moderateVerticalScale(90),
        width: Width
    },

    ButtonView: {
        width: Width * 0.80,
        backgroundColor: colorConstant.buttonColor,
        alignSelf: "center",
        marginTop: moderateVerticalScale(20),
        borderRadius: 50,
        padding: moderateVerticalScale(15)
    },
    SaveText: {
        fontFamily: fontConstant.bold,
        fontSize: 16,
        color: colorConstant.white,
        textAlign: "center"

    }
})