import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import React, {useState} from 'react';
import {
  colorConstant,
  fontConstant,
  imageConstants,
  shadowObject,
} from '../utils/constants';
import CustomInput from '../Custom/CustomInput';
import CustomItem from '../Custom/CustomItem';
import CustomTotal from '../Custom/CustomTotal';
import {moderateScale, moderateVerticalScale} from 'react-native-size-matters';
import {Width} from '../dimensions/dimensions';
import CustomButton from '../Custom/CustomButton';
import CustomCancelModal from '../Custom/CustomCancelModal';
import CustomHeaderText from '../Custom/CustomHeaderText';

const HistoryDetailScreen = props => {
  const [isModalVisible, setModalVisible] = useState(false);
  const {tabIndex, screenName} = props.route.params ?? '';
  // console.log(screenName , "tabindex");
  const toggleModal = () => {
    console.log('logout working');
    setModalVisible(!isModalVisible);
  };

  const ResetFunction = () => {
    setModalVisible(false);
  };

  return (
    <View style={styles.main}>
      <CustomHeaderText
        arrowIcon={imageConstants.BackIcon}
        headTitle={'Leslie Alexander'}
        onPress={() => props.navigation.goBack()}
      />

      <ScrollView
        contentContainerStyle={{
          paddingBottom: moderateVerticalScale(100),
        }}>
        <CustomItem
          unpaid={false}
          phonecolor={colorConstant.orange}
          timecolor={colorConstant.orange}
        />

        <CustomTotal
          title1={'Lab Test Fee'}
          title2={'Coupon Discount'}
          title3={'Collection fee (incl. Consumables & Transportation Fees)'}
          title4={'Visit Charges (Sample not collected)'}
          color={colorConstant.red}
          payment={true}
        />

        <View
          style={{
            flexDirection: 'row',
            backgroundColor: colorConstant.white,
            padding: moderateVerticalScale(10),
            borderColor: colorConstant.borderColor,
            borderWidth: 0.8,
            width: '90%',
            alignSelf: 'center',
            borderRadius: 20,
            marginTop: moderateVerticalScale(20),
          }}>
          <Image
            source={imageConstants.DogImg}
            resizeMode="contain"
            style={{
              width: 70,
              height: 70,
            }}
          />
          <View style={styles.SubContainer}>
            <Text
              style={{
                fontFamily: fontConstant.bold,
                color: colorConstant.black,
                fontSize: 14,
                lineHeight: 30,
              }}>
              Rocky
            </Text>

            <Text
              style={{
                fontFamily: fontConstant.medium,
                color: colorConstant.black,
                fontSize: 13,
                lineHeight: 20,
              }}>
              Chihuahua
            </Text>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                // backgroundColor:"pink",
                width: '70%',
                right: '10%',
                justifyContent: 'space-evenly',
              }}>
              <Text
                style={{
                  fontFamily: fontConstant.medium,
                  color: colorConstant.black,
                  fontSize: 13,
                }}>
                5 kg
              </Text>

              <View
                style={{
                  width: 4,
                  height: 4,
                  backgroundColor: '#000',
                  borderRadius: 100,
                  marginTop: moderateVerticalScale(2),
                }}
              />

              <Text
                style={{
                  fontFamily: fontConstant.medium,
                  color: colorConstant.black,
                  fontSize: 13,
                }}>
                8 years old
              </Text>
            </View>
          </View>
        </View>

        <View
          style={{
            backgroundColor: colorConstant.white,
            padding: moderateVerticalScale(10),
            borderColor: colorConstant.borderColor,
            borderWidth: 0.8,
            width: '90%',
            alignSelf: 'center',
            borderRadius: 20,
            marginTop: moderateVerticalScale(20),
          }}>
          <Text
            style={{
              fontFamily: fontConstant.bold,
              color: colorConstant.black,
              fontSize: 14,
              lineHeight: 30,
              paddingHorizontal: moderateScale(10),
            }}>
            Lab Test
          </Text>

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              width: '95%',
              alignSelf: 'center',
            }}>
            <Text style={styles.labSubText}>1. Rabies</Text>
            <Text style={styles.amountText}>₹ 500</Text>
          </View>

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              width: '95%',
              alignSelf: 'center',
            }}>
            <Text style={styles.labSubText}>2. Rabies</Text>
            <Text style={styles.amountText}>₹ 500</Text>
          </View>

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              width: '95%',
              alignSelf: 'center',
            }}>
            <Text style={styles.labSubText}>3. Rabies</Text>
            <Text style={styles.amountText}>₹ 500</Text>
          </View>
        </View>

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: colorConstant.white,
            padding: moderateVerticalScale(10),
            // borderColor: colorConstant.borderColor,
            // borderWidth: 0.8,
            width: '90%',
            alignSelf: 'center',
            justifyContent: 'space-between',
            borderRadius: 20,
            marginTop: moderateVerticalScale(20),
            ...shadowObject,
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Image
              source={imageConstants.prescription}
              resizeMode="contain"
              style={{
                width: 50,
                height: 50,
              }}
            />
            <Text
              style={{
                fontFamily: fontConstant.bold,
                color: colorConstant.black,
                fontSize: 14,
                paddingHorizontal: moderateScale(10),
              }}>
              Prescription
            </Text>
          </View>

          <Image
            source={imageConstants.eye}
            resizeMode="contain"
            style={{
              width: 20,
              height: 20,
              paddingRight: '10%',
            }}
          />
        </View>
      </ScrollView>

      <CustomCancelModal
        isModalVisible={isModalVisible}
        setModalVisible={setModalVisible}
        OnButtonCancel={ResetFunction}
      />
    </View>
  );
};

export default HistoryDetailScreen;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: '#EDF7FF',
  },

  BottomView: {
    position: 'absolute',
    bottom: 0,
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    backgroundColor: colorConstant.white,
    height: moderateVerticalScale(90),
    width: Width,
    // backgroundColor:"pink"
  },

  BottomView1: {
    position: 'absolute',
    bottom: 0,
    alignSelf: 'center',
    backgroundColor: colorConstant.white,
    height: moderateVerticalScale(90),
    width: Width,
    // backgroundColor:"pink"
  },

  SubContainer: {
    paddingHorizontal: moderateScale(10),
  },

  labSubText: {
    fontFamily: fontConstant.medium,
    fontSize: 14,
    color: colorConstant.black,
    marginTop: moderateVerticalScale(8),
  },

  amountText: {
    fontFamily: fontConstant.regular,
    fontSize: 13,
    color: colorConstant.buttonColor,
  },

  DocContainer: {
    backgroundColor: '#EFF4FF',
    width: '90%',
    height: moderateVerticalScale(70),
    marginTop: moderateVerticalScale(20),
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    borderColor: '#0174CC',
    borderWidth: 0.5,
    flexDirection: 'row',
    alignItems: 'center',
  },

  DocText: {
    fontFamily: fontConstant.bold,
    fontSize: 14,
    paddingHorizontal: moderateScale(10),
    color: colorConstant.black,
  },
});
