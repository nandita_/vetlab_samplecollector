import { FlatList, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import SegmentedControl from '../Custom/SegmentComponent'
import { colorConstant, fontConstant, imageConstants } from '../utils/constants'
import CustomHeader from '../Custom/CustomHeader'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import Wrapper from '../Custom/wrapper'

const Homepage = (props) => {
    const [tabIndex, setTabIndex] = React.useState(0);
    console.log(tabIndex , "TBABBBBA");

    const data = [
        {
            id: 1,
            profileImage: imageConstants.profil,
            name: "Leslie Alexander",
            subName: "Rocky",
            sector: "Sector 70 NOIDA",
            phone: "+91 1234567890",
            time: "10:30 am",
            timeImage: imageConstants.Time,
            phoneImage: imageConstants.phone1

        },
        {
            id: 2,
            profileImage: imageConstants.profil1,
            name: "Leslie Alexander",
            subName: "Rocky",
            sector: "Sector 70 NOIDA",
            phone: "+91 1234567890",
            time: "10:30 am",
            timeImage: imageConstants.Time,
            phoneImage: imageConstants.phone1

        },
        {
            id: 3,
            profileImage: imageConstants.profil2,
            name: "Leslie Alexander",
            subName: "Rocky",
            sector: "Sector 70 NOIDA",
            phone: "+91 1234567890",
            time: "10:30 am",
            timeImage: imageConstants.Time,
            phoneImage: imageConstants.phone1
        }
    ]

    const _renderData = ({ item, index }) => {
        return (
            <TouchableOpacity style={styles.ListContainer} activeOpacity={0.8} 
            onPress={() => {
                if(tabIndex === 0) {
                    
                    props.navigation.navigate("DetailScreen")
                }
                else{
                    props.navigation.navigate("HistoryDetailScreen")
                }
            
            }
            }
                
                >

                <View style={styles.subcontainer}>
                
                    <Image
                        source={item.profileImage}
                        resizeMode='contain'
                        style={styles.imageStyle}
                    />
                   

                    <View style={{ paddingHorizontal: moderateScale(15) }}>
                        {
                            tabIndex === 1 ? 
                            <View style={{
                                flexDirection:"row",
                                alignItems:"center",
                                justifyContent:"space-between"
                            }}>
                                 <Text style={styles.nameText}>{item.name}</Text>
                                 <Text style={styles.CompleteText}>Completed</Text>
                            </View>
                            :
                            <Text style={styles.nameText}>{item.name}</Text>
                        }
                        

                        <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-evenly", width: "75%", right: "15%" }}>
                            <Text style={styles.subnameText}>{item.subName}</Text>
                            <View style={{ width: 4, height: 4, backgroundColor: "#134094", borderRadius: 100 }} />
                            <Text style={styles.subnameText}>{item.sector}</Text>
                        </View>

                        <View style={{ width: "80%", borderBottomWidth: 1, borderBottomColor: "#E1F2FF", marginTop: moderateVerticalScale(4), marginBottom: moderateVerticalScale(4) }} />

                        <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-evenly", width: "88%",right: "2%" }}>
                            <Image source={item.phoneImage}
                                resizeMode='contain'
                                style={{
                                    width: 12,
                                    height: 12
                                }}
                            />
                            <Text style={styles.PhoneText}>{item.phone}</Text>
                            <Image source={item.timeImage}
                                resizeMode='contain'
                                style={{
                                    width: 12,
                                    marginLeft:"8%",
                                    height: 12
                                }}
                            />
                            <Text style={styles.PhoneText}>{item.time}</Text>
                        </View>
                    </View>

                </View>
            </TouchableOpacity>
        ) 
    }
    const handleTabsChange = index => {
        setTabIndex(index);
       
    };
    return (
            <Wrapper>
        <View style={styles.main}>
            <CustomHeader
            // onPress={() => props.navigation.navigate("DrawerScreen")}
            onPress={() => {props.navigation.openDrawer()}}
                arrowIcon={imageConstants.menu}
                headerImage={true}
                Logo={imageConstants.HomeLogo}
                NotifyButton={() => props.navigation.navigate("Notification")}
                notify={imageConstants.bell}
                notifyImage={true}
            />

            <View style={styles.SegmentContainer}>
                <SegmentedControl
                    tabs={['Upcoming Sample Collection', 'Sample Collection History']}
                    currentIndex={tabIndex}
                    onChange={handleTabsChange}
                    segmentedControlBackgroundColor={colorConstant.white}
                    activeSegmentBackgroundColor="#FF8C00"
                    activeTextColor={colorConstant.white}
                    textColor="black"
                    paddingVertical={18}
                />
            </View>
            <ScrollView style={styles.Container}>
                <Text style={styles.DateText}>28 Dec, 2023</Text>

                <Text style={styles.SectorText}>Sector 63, noida</Text>

                <FlatList
                    data={data}
                    renderItem={_renderData}
                    key={item => item.id}
                />

                <Text style={styles.SectorText}>Sector 64, noida</Text>

                <FlatList
                    data={data}
                    renderItem={_renderData}
                    key={item => item.id}
                    contentContainerStyle={{
                        paddingBottom: moderateVerticalScale(40)
                    }}
                />

            </ScrollView>
        </View>
             </Wrapper>
    )
}

export default Homepage

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: "#EDF7FF"
    },
    SegmentContainer: {
        alignSelf: "center",
        marginTop: moderateVerticalScale(12)
    },
    Container: {
        paddingHorizontal: moderateVerticalScale(20),
       
    },
    DateText: {
        fontFamily: fontConstant.bold,
        fontSize: 14,
        color: colorConstant.black,

    },
    SectorText: {
        fontFamily: fontConstant.medium,
        fontSize: 14,
        color: colorConstant.buttonColor,
        marginTop: moderateVerticalScale(15)
    },
    ListContainer: {
        backgroundColor: colorConstant.white,
        borderWidth: 0.5,
        borderColor: colorConstant.borderColor,
        padding: moderateVerticalScale(15),
        marginTop: moderateVerticalScale(15),
        borderRadius: 18
    },
    imageStyle: {
        width: 70,
        height: 70
    },

    subcontainer: {
        flexDirection: "row",
        alignItems: "center",
    },

    nameText: {
        fontFamily: fontConstant.bold,
        fontSize: 14,
        lineHeight: 22,
        color: colorConstant.black
    },

    subnameText: {
        fontFamily: fontConstant.medium,
        fontSize: 12,
        lineHeight: 20,
        color: "#134094"
    },
    PhoneText: {
        fontFamily: fontConstant.medium,
        fontSize: 12,
        lineHeight: 20,
        color: "#EC6602",
        marginLeft:"2%",
    },

    CompleteText:{
        fontFamily: fontConstant.medium,
        fontSize:12, 
        right:10,
        color: colorConstant.green
    }
})