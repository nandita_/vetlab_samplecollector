import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import {
  colorConstant,
  fontConstant,
  imageConstants,
  shadowObject,
} from '../utils/constants';
import CustomInput from '../Custom/CustomInput';
import CustomItem from '../Custom/CustomItem';
import CustomTotal from '../Custom/CustomTotal';
import {moderateScale, moderateVerticalScale} from 'react-native-size-matters';
import {Width} from '../dimensions/dimensions';
import CustomButton from '../Custom/CustomButton';
import CustomCancelModal from '../Custom/CustomCancelModal';
import CustomHeaderText from '../Custom/CustomHeaderText';

const PendingHomeCollection = props => {
  const refRBSheet = useRef();
  const [isModalVisible, setModalVisible] = useState(false);
  const {tabIndex, screenName} = props.route.params ?? '';
  // console.log(screenName , "tabindex");
  const toggleModal = () => {
    // console.log('logout working');
    setModalVisible(!isModalVisible);
  };

  const data = [
    {
      id: 1,
      profileImage: imageConstants.profil,
      name: 'Leslie Alexander',
      subName: 'Rocky',
      sector: 'Sector 70 NOIDA',
      phone: '+91 1234567890',
      time: '10:30 am',
      timeImage: imageConstants.Time,
      phoneImage: imageConstants.phone1,
    },
    {
      id: 2,
      profileImage: imageConstants.profil1,
      name: 'Leslie Alexander',
      subName: 'Rocky',
      sector: 'Sector 70 NOIDA',
      phone: '+91 1234567890',
      time: '10:30 am',
      timeImage: imageConstants.Time,
      phoneImage: imageConstants.phone1,
    },
    {
      id: 3,
      profileImage: imageConstants.profil2,
      name: 'Leslie Alexander',
      subName: 'Rocky',
      sector: 'Sector 70 NOIDA',
      phone: '+91 1234567890',
      time: '10:30 am',
      timeImage: imageConstants.Time,
      phoneImage: imageConstants.phone1,
    },
  ];

  const _renderData = ({item, index}) => {
    return (
      <TouchableOpacity
        style={styles.ListContainer}
        activeOpacity={0.8}
        onPress={() => {
          // if (tabIndex === 0) {
          props.navigation.navigate('DetailScreen');
          // } else {
          //   props.navigation.navigate('HistoryDetailScreen');
          // }
        }}>
        <View style={styles.subcontainer}>
          <Image
            source={item.profileImage}
            resizeMode="contain"
            style={styles.imageStyle}
          />

          <View style={{paddingHorizontal: moderateScale(15)}}>
            {tabIndex === 1 ? (
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <Text style={styles.nameText}>{item.name}</Text>
                <Text style={styles.CompleteText}>Completed</Text>
              </View>
            ) : (
              <Text style={styles.nameText}>{item.name}</Text>
            )}

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-evenly',
                width: '75%',
                right: '15%',
              }}>
              <Text style={styles.subnameText}>{item.subName}</Text>
              <View
                style={{
                  width: 4,
                  height: 4,
                  backgroundColor: '#134094',
                  borderRadius: 100,
                }}
              />
              <Text style={styles.subnameText}>{item.sector}</Text>
            </View>

            <View
              style={{
                width: '80%',
                borderBottomWidth: 1,
                borderBottomColor: '#E1F2FF',
                marginTop: moderateVerticalScale(4),
                marginBottom: moderateVerticalScale(4),
              }}
            />

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-evenly',
                width: '88%',
                right: '2%',
              }}>
              <Image
                source={item.phoneImage}
                resizeMode="contain"
                style={{
                  width: 12,
                  height: 12,
                }}
              />
              <Text style={styles.PhoneText}>{item.phone}</Text>
              <Image
                source={item.timeImage}
                resizeMode="contain"
                style={{
                  width: 12,
                  marginLeft: '8%',
                  height: 12,
                }}
              />
              <Text style={styles.PhoneText}>{item.time}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.main}>
      <CustomHeaderText
        arrowIcon={imageConstants.BackIcon}
        headTitle={'Upcoming Sample Collection'}
        onPress={() => props.navigation.goBack()}
      />

      <ScrollView style={styles.Container}>
        <Text style={styles.DateText}>28 Dec, 2023</Text>

        <Text style={styles.SectorText}>Sector 63, noida</Text>

        <FlatList data={data} renderItem={_renderData} key={item => item.id} />

        <Text style={styles.SectorText}>Sector 64, noida</Text>

        <FlatList
          data={data}
          renderItem={_renderData}
          key={item => item.id}
          contentContainerStyle={{
            paddingBottom: moderateVerticalScale(40),
          }}
        />
      </ScrollView>
    </View>
  );
};

export default PendingHomeCollection;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: '#EDF7FF',
  },
  SegmentContainer: {
    alignSelf: 'center',
    marginTop: moderateVerticalScale(12),
  },
  Container: {
    paddingHorizontal: moderateVerticalScale(20),
  },
  DateText: {
    marginTop: moderateVerticalScale(20),
    fontFamily: fontConstant.bold,
    fontSize: 14,
    color: colorConstant.black,
  },
  SectorText: {
    fontFamily: fontConstant.medium,
    fontSize: 14,
    color: colorConstant.buttonColor,
    marginTop: moderateVerticalScale(15),
  },
  ListContainer: {
    backgroundColor: colorConstant.white,
    borderWidth: 0.5,
    borderColor: colorConstant.borderColor,
    padding: moderateVerticalScale(15),
    marginTop: moderateVerticalScale(15),
    borderRadius: 18,
  },
  imageStyle: {
    width: 70,
    height: 70,
  },

  subcontainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  nameText: {
    fontFamily: fontConstant.bold,
    fontSize: 14,
    lineHeight: 22,
    color: colorConstant.black,
  },

  subnameText: {
    fontFamily: fontConstant.medium,
    fontSize: 12,
    lineHeight: 20,
    color: '#134094',
  },
  PhoneText: {
    fontFamily: fontConstant.medium,
    fontSize: 12,
    lineHeight: 20,
    color: '#EC6602',
    marginLeft: '2%',
  },

  CompleteText: {
    fontFamily: fontConstant.medium,
    fontSize: 12,
    right: 10,
    color: colorConstant.green,
  },
});
