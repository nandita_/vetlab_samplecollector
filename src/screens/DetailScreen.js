import { StyleSheet, Text, View, Image, ScrollView, TouchableOpacity } from 'react-native'
import React, { useEffect, useRef, useState } from 'react'
import { colorConstant, fontConstant, imageConstants, shadowObject } from '../utils/constants'
import CustomInput from '../Custom/CustomInput'
import CustomItem from '../Custom/CustomItem'
import CustomTotal from '../Custom/CustomTotal'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import { Width } from '../dimensions/dimensions'
import CustomButton from '../Custom/CustomButton'
import CustomCancelModal from '../Custom/CustomCancelModal'
import CustomHeaderText from '../Custom/CustomHeaderText'



const DetailScreen = (props) => {
    const refRBSheet = useRef();
    const [isModalVisible, setModalVisible] = useState(false);
    const { tabIndex, screenName } = props.route.params ?? "";
    // console.log(screenName , "tabindex");
    const toggleModal = () => {
        // console.log('logout working');
        setModalVisible(!isModalVisible);
    };


    return (
        <View style={styles.main}>

            <CustomHeaderText
                arrowIcon={imageConstants.BackIcon}
                headTitle={"Leslie Alexander"}
                onPress={() => props.navigation.goBack()}
            />

            <ScrollView contentContainerStyle={{
                paddingBottom: moderateVerticalScale(100)
            }}>
                <CustomItem
                    unpaid={tabIndex === 1 ? false : true}

                    phonecolor={colorConstant.orange}
                    timecolor={colorConstant.orange}
                />

                <CustomTotal
                    title1={"Lab Test Fee"}
                    title2={"Coupon Discount"}
                    title3={"Collection fee (incl. Consumables & Transportation Fees)"}
                    title4={"Visit Charges (Sample not collected)"}
                    color={tabIndex === 1 && colorConstant.red}
                />

                <View style={{
                    flexDirection: "row",
                    backgroundColor: colorConstant.white,
                    padding: moderateVerticalScale(10),
                    borderColor: colorConstant.borderColor,
                    borderWidth: 0.8,
                    width: "90%",
                    alignSelf: "center",
                    borderRadius: 20,
                    marginTop: moderateVerticalScale(20)
                }}>
                    <Image
                        source={imageConstants.DogImg}
                        resizeMode="contain"
                        style={{
                            width: 70,
                            height: 70
                        }}
                    />
                    <View style={styles.SubContainer}>
                        <Text style={{
                            fontFamily: fontConstant.bold,
                            color: colorConstant.black,
                            fontSize: 14,
                            lineHeight: 30,
                        }}>
                            Rocky
                        </Text>

                        <Text style={{
                            fontFamily: fontConstant.medium,
                            color: colorConstant.black,
                            fontSize: 13,
                            lineHeight: 20

                        }}>
                            Chihuahua
                        </Text>

                        <View style={{
                            flexDirection: "row",
                            alignItems: "center",
                            // backgroundColor:"pink",
                            width: "70%",
                            right: "10%",
                            justifyContent: "space-evenly"
                        }}>
                            <Text style={{

                                fontFamily: fontConstant.medium,
                                color: colorConstant.black,
                                fontSize: 13

                            }}>
                                5 kg
                            </Text>

                            <View style={{ width: 4, height: 4, backgroundColor: "#000", borderRadius: 100, marginTop: moderateVerticalScale(2) }} />

                            <Text style={{

                                fontFamily: fontConstant.medium,
                                color: colorConstant.black,
                                fontSize: 13

                            }}>
                                8 years old
                            </Text>
                        </View>
                    </View>
                </View>

                <View style={{

                    backgroundColor: colorConstant.white,
                    padding: moderateVerticalScale(10),
                    borderColor: colorConstant.borderColor,
                    borderWidth: 0.8,
                    width: "90%",
                    alignSelf: "center",
                    borderRadius: 20,
                    marginTop: moderateVerticalScale(20)
                }}>

                    <Text style={{
                        fontFamily: fontConstant.bold,
                        color: colorConstant.black,
                        fontSize: 14,
                        lineHeight: 30,
                        paddingHorizontal: moderateScale(10)
                    }} >Lab Test</Text>

                    <View style={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "space-between",
                        width: "95%",
                        alignSelf: "center"
                    }}>
                        <Text style={styles.labSubText}>1. Rabies</Text>
                        <Text style={styles.amountText}>₹ 500</Text>

                    </View>

                    <View style={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "space-between",
                        width: "95%",
                        alignSelf: "center"
                    }}>
                        <Text style={styles.labSubText}>2. Rabies</Text>
                        <Text style={styles.amountText}>₹ 500</Text>

                    </View>

                    <View style={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "space-between",
                        width: "95%",
                        alignSelf: "center"
                    }}>
                        <Text style={styles.labSubText}>3. Rabies</Text>
                        <Text style={styles.amountText}>₹ 500</Text>

                    </View>

                </View>

                <View style={{
                    // flexDirection:"row",
                    // alignItems:"center",
                    backgroundColor: colorConstant.white,
                    padding: moderateVerticalScale(15),
                    borderColor: colorConstant.borderColor,
                    borderWidth: 0.8,
                    width: "90%",
                    alignSelf: "center",

                    borderRadius: 20,
                    marginTop: moderateVerticalScale(20)
                }}>
                    <Text style={{
                        fontFamily: fontConstant.bold,
                        color: colorConstant.black,
                        fontSize: 14,
                        paddingHorizontal: moderateScale(10)
                    }}>VetLab Basic Health Checkup</Text>
                    <View style={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "space-between",
                        marginTop: moderateVerticalScale(10),

                    }}>

                        <View style={{
                            flexDirection: "row",
                            alignItems: "center",
                            paddingHorizontal: moderateScale(10)
                        }}>
                            <Image
                                source={imageConstants.flask}
                                resizeMode='contain'
                                style={{
                                    width: 20,
                                    height: 20
                                }}
                            />
                            <Text style={{
                                fontFamily: fontConstant.medium,
                                color: colorConstant.black,
                                fontSize: 14,
                                paddingHorizontal: moderateScale(10)
                            }}>59 Lab Tests</Text>
                        </View>


                        <Text style={styles.amountText}>₹ 500</Text>
                    </View>
                </View>

                <View style={{
                    flexDirection: "row",
                    alignItems: "center",
                    backgroundColor: colorConstant.white,
                    padding: moderateVerticalScale(10),
                    // borderColor: colorConstant.borderColor,
                    // borderWidth: 0.8,
                    width: "90%",
                    alignSelf: "center",
                    justifyContent: "space-between",
                    borderRadius: 20,
                    marginTop: moderateVerticalScale(20),
                    ...shadowObject
                }}>
                    <View style={{
                        flexDirection: "row",
                        alignItems: "center"
                    }}>
                        <Image
                            source={imageConstants.prescription}
                            resizeMode='contain'
                            style={{
                                width: 50,
                                height: 50
                            }}
                        />
                        <Text style={{
                            fontFamily: fontConstant.bold,
                            color: colorConstant.black,
                            fontSize: 14,
                            paddingHorizontal: moderateScale(10)
                        }}>Prescription</Text>
                    </View>

                    <Image
                        source={imageConstants.eye}
                        resizeMode='contain'
                        style={{
                            width: 20,
                            height: 20,
                            paddingRight: "10%"
                        }}
                    />

                </View>


                <View style={{
                    backgroundColor: colorConstant.white,
                    padding: moderateVerticalScale(10),
                    // borderColor: colorConstant.borderColor,
                    // borderWidth: 0.8,
                    width: "90%",
                    alignSelf: "center",
                    borderRadius: 20,
                    marginTop: moderateVerticalScale(20),
                    ...shadowObject
                }}>
                    <View style={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "space-between",
                    }}>

                        <Text style={{
                            fontFamily: fontConstant.bold,
                            color: colorConstant.buttonColor,
                            fontSize: 14,
                            paddingHorizontal: moderateScale(10)
                        }}>Test Name</Text>
                        <Text style={{
                            fontFamily: fontConstant.regular,
                            color: colorConstant.black,
                            fontSize: 14,

                        }}>Rabies</Text>
                    </View>

                    <View style={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "space-between",
                        marginTop: moderateVerticalScale(10)
                    }}>

                        <Text style={{
                            fontFamily: fontConstant.bold,
                            color: colorConstant.buttonColor,
                            fontSize: 14,
                            lineHeight: 20,
                            paddingHorizontal: moderateScale(10)
                        }}>Type of {'\n'}Sample Collected</Text>
                        <Text style={{
                            fontFamily: fontConstant.regular,
                            color: colorConstant.black,
                            fontSize: 14,

                        }}>EDAT Blood</Text>
                    </View>

                    <View style={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "space-between",
                        marginTop: moderateVerticalScale(10)
                    }}>

                        <Text style={{
                            fontFamily: fontConstant.bold,
                            color: colorConstant.buttonColor,
                            fontSize: 14,
                            paddingHorizontal: moderateScale(10)
                        }}>Capture Sample UID</Text>
                        <Image
                            source={imageConstants.eye}
                            resizeMode='contain'
                            style={{
                                width: 20,
                                height: 20,
                                paddingRight: "10%"
                            }}
                        />
                    </View>



                </View>

                <View style={{
                    backgroundColor: colorConstant.white,
                    padding: moderateVerticalScale(10),
                    // borderColor: colorConstant.borderColor,
                    // borderWidth: 0.8,
                    width: "90%",
                    alignSelf: "center",
                    borderRadius: 20,
                    marginTop: moderateVerticalScale(20),
                    ...shadowObject
                }}>
                    <View style={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "space-between",
                    }}>

                        <Text style={{
                            fontFamily: fontConstant.bold,
                            color: colorConstant.buttonColor,
                            fontSize: 14,
                            paddingHorizontal: moderateScale(10)
                        }}>Test Name</Text>
                        <Text style={{
                            fontFamily: fontConstant.regular,
                            color: colorConstant.black,
                            fontSize: 14,

                        }}>Rabies</Text>
                    </View>

                    <View style={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "space-between",
                        marginTop: moderateVerticalScale(10)
                    }}>

                        <Text style={{
                            fontFamily: fontConstant.bold,
                            color: colorConstant.buttonColor,
                            fontSize: 14,
                            lineHeight: 20,
                            paddingHorizontal: moderateScale(10)
                        }}>Type of {'\n'}Sample Collected</Text>
                        <Text style={{
                            fontFamily: fontConstant.regular,
                            color: colorConstant.black,
                            fontSize: 14,
                            textAlign: "right"

                        }}>Sample Not {'\n'}Collected</Text>
                    </View>

                    <View style={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "space-between",
                        marginTop: moderateVerticalScale(10)
                    }}>

                        <Text style={{
                            fontFamily: fontConstant.bold,
                            color: colorConstant.buttonColor,
                            fontSize: 14,
                            paddingHorizontal: moderateScale(10)
                        }}>Reason</Text>
                        <Text style={{
                            fontFamily: fontConstant.regular,
                            color: colorConstant.black,
                            fontSize: 14,

                        }}>Reason</Text>
                    </View>



                </View>

                <Text style={{
                    paddingHorizontal: moderateScale(20),
                    marginTop: moderateVerticalScale(15),
                    fontFamily: fontConstant.medium,
                    fontSize: 16,
                    color: colorConstant.black
                }}>Upload Prescription(If referred by vet)</Text>

                <TouchableOpacity style={styles.DocContainer} activeOpacity={0.8} >

                    <Image source={imageConstants.Icon}
                        resizeMode='contain'
                        style={{
                            width: 20,
                            height: 20,
                            alignSelf: "center"
                        }}

                    />

                    <Text style={styles.DocText}>Upload Image</Text>
                </TouchableOpacity>


            </ScrollView>

            {
                screenName === "SampleCollection" || screenName === "ScreenShotImage"?
                <View style={styles.BottomView1}>

                <CustomButton
                    OnButtonPress={() => {
                        if(screenName=== "ScreenShotImage"){

                            props.navigation.navigate("DrawerStack")
                        }
                        else if(screenName === "SampleCollection"){
                            props.navigation.navigate("Appointment")
                        }
                    
                    }}
                    buttonText={screenName === "ScreenShotImage" ? "Submit" : "Proceed for Payment"}
                    width={"90%"}
                    bottom={10}
                    color={colorConstant.white}
                />
            </View>
           :
                    <View style={styles.BottomView}>
                        <CustomButton
                            width={"45%"}
                            OnButtonPress={() => toggleModal()}
                            bottom={10}
                            backgroundColor={"#E1F2FF"}
                            buttonText={"Cancel"}
                            color={colorConstant.buttonColor}
                        />

                        <CustomButton
                            OnButtonPress={() => props.navigation.navigate("SampleCollection")}
                            buttonText={"Proceed"}
                            width={"45%"}
                            bottom={10}
                            color={colorConstant.white}
                        />
                    </View>
                    

                    
                       



            }


            <CustomCancelModal
                isModalVisible={isModalVisible}
                setModalVisible={setModalVisible}
                OnButtonPress={() => props.navigation.navigate("DrawerStack")}
            />

        </View>
    )
}

export default DetailScreen

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: "#EDF7FF"
    },

    BottomView: {
        position: "absolute",
        bottom: 0,
        alignSelf: "center",
        flexDirection: "row",
        justifyContent: "space-evenly",
        alignItems: "center",
        backgroundColor: colorConstant.white,
        height: moderateVerticalScale(90),
        width: Width,
        // backgroundColor:"pink"
    },

    BottomView1: {
        position: "absolute",
        bottom: 0,
        alignSelf: "center",
        backgroundColor: colorConstant.white,
        height: moderateVerticalScale(90),
        width: Width,
        // backgroundColor:"pink"
    },

    SubContainer: {
        paddingHorizontal: moderateScale(10)
    },

    labSubText: {
        fontFamily: fontConstant.medium,
        fontSize: 14,
        color: colorConstant.black,
        marginTop: moderateVerticalScale(8)
    },

    amountText: {
        fontFamily: fontConstant.regular,
        fontSize: 13,
        color: colorConstant.buttonColor
    },

    DocContainer: {
        backgroundColor: "#EFF4FF",
        width: "90%",
        height: moderateVerticalScale(70),
        marginTop: moderateVerticalScale(20),
        alignSelf: "center",
        justifyContent: "center",
        borderRadius: 10,
        borderColor: "#0174CC",
        borderWidth: 0.5,
        flexDirection: "row",
        alignItems: "center"
    },

    DocText: {
        fontFamily: fontConstant.bold,
        fontSize: 14,
        paddingHorizontal: moderateScale(10),
        color: colorConstant.black
    },

})