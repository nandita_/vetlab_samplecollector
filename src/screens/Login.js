import { Image, KeyboardAvoidingView, Platform, StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useRef, useState } from 'react'
import { MARGIN_BOTTOM, colorConstant, fontConstant, imageConstants } from '../utils/constants'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import CustomText from '../Custom/CustomText'
import CustomInput from '../Custom/CustomInput'
import CustomButton from '../Custom/CustomButton'
import { useDispatch, useSelector } from 'react-redux'
import { setIntroStatus } from '../redux/ParentFlow'
import CustomHeaderText from '../Custom/CustomHeaderText'
import {KeyboardAwareScrollView} from '@codler/react-native-keyboard-aware-scroll-view';
import { store } from '../utils/store'
import {DeviceWidth, Width } from '../dimensions/dimensions'

const Login = (props) => {
  // const introStatus = useSelector((state) => state.counter.counter.introStatus)
  const dispatch = useDispatch()
  const [padding, setPadding] = useState(20);
  const reff =  useRef(null)
  useEffect(() => {
    setPadding(200);
  }, []);
console.log("devive",DeviceWidth);
  return (
    <KeyboardAvoidingView 
    //  behavior={Platform.OS == "ios"  ? "padding" : "height"}
     style={styles.main}>
      {/* <CustomHeaderText
        onPress={() => props.navigation.goBack()}
      /> */}

      <KeyboardAwareScrollView
      // ref={reff}
      // onContentSizeChange={()=>{
      //   reff.scrollToEnd({animated:true})
      // }}
      contentContainerStyle={{
        paddingBottom:moderateVerticalScale(20),
      }}
        // extraHeight={moderateScale(100)}
        >
        <Text style={styles.LoginText}>Login</Text>

        <Image
          source={imageConstants.Logo}
          resizeMode='contain'
          style={styles.LoginLogo}
        />

        <Text style={styles.subtext}>Welcome to Vetlab EZiconnect</Text>

        <CustomText
          color={colorConstant.textColor}
          Title={"Username"}

        />

        <CustomInput
          padding={moderateScale(15)}
          borderRadius={15}
          source={imageConstants.userIcon}
          placeholder={"Username"}
        />

        <CustomText
          color={colorConstant.textColor}
          marginTop={moderateVerticalScale(25)}
          Title={"Password"}

        />

        <CustomInput
          padding={moderateScale(15)}
          borderRadius={15}
          source={imageConstants.lockIcon}
          placeholder={"Password"}
        />
      </KeyboardAwareScrollView>
       

      <View style={{
        // paddingHorizontal: moderateScale(10),
        width: DeviceWidth * 0.90,
        alignSelf: "center",
        // alignItems:"center",
        // backgroundColor:"red",
        flexDirection: "row",
        // marginTop: moderateVerticalScale(100),
        marginVertical: moderateVerticalScale(35),
      }}>
        <Image
          source={imageConstants.checkbox}
          resizeMode='contain'
          style={{
            width: 20,
            height: 20,
            margin:5
          }}
        />
        <Text style={styles.agreeText}>I agree to VetLab’s <Text style={{color: colorConstant.yellow}}>Terms & Conditions</Text> and {"\n"}<Text style={{color: colorConstant.yellow}}> Privacy Policy</Text></Text>
      </View>
      <CustomButton
        OnButtonPress={() => {
          // props.navigation.navigate("DrawerStack")
          store.dispatch(setIntroStatus({ introStatus: "main" }))
        }}
        // position={"absolute"}
        bottom={MARGIN_BOTTOM}
        buttonText={"Login"}
        borderRadius={15}
        color={colorConstant.white}

      />
    </KeyboardAvoidingView>
  )
}

export default Login

const styles = StyleSheet.create({
  main: {
    backgroundColor:"white",
    flex: 1,
  },

  LoginLogo: {
    width: Width*0.40,
    height: 70,
    alignSelf: "center",
    marginTop: moderateVerticalScale(10)
  },
  LoginText: {
    fontFamily: fontConstant.semiBold,
    fontSize: 20,
    color: colorConstant.textColor,
    textAlign: "center",
    marginTop: moderateVerticalScale(50)
  },

  subtext: {
    fontFamily: fontConstant.medium,
    fontSize: 14,
    color: colorConstant.secondaryText,
    textAlign: "center",
    marginTop: moderateVerticalScale(8)
  },

  agreeText:{
    paddingHorizontal: moderateScale(8),
    lineHeight:22,
    fontFamily: fontConstant.medium,
    color: colorConstant.textColor,
    fontSize:14,
  }
})