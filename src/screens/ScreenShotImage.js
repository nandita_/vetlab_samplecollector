import { Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import CustomHeaderText from '../Custom/CustomHeaderText'
import { colorConstant, imageConstants } from '../utils/constants'
import { moderateVerticalScale } from 'react-native-size-matters'
import CustomButton from '../Custom/CustomButton'
import { Height, Width } from '../dimensions/dimensions'


const ScreenShotImage = (props) => {
    const screenName ="ScreenShotImage";
  return (
    <View style={styles.main}>
    <CustomHeaderText
        arrowIcon={imageConstants.BackIcon}
        headTitle={"Screenshot"}
        // width={"60%"}
        onPress={() => props.navigation.goBack()}
    />

    <View style={{height: Height, backgroundColor: '#00000080', }}>
     <Image 
     source={imageConstants.screenshotImage}
     resizeMode='contain'
     style={{
        width:Width*0.90,
        height:moderateVerticalScale(550),
        // backgroundColor:"pink",
        alignSelf:"center"
     }}
     />
    </View>
    
    <View style={styles.BottomView}>
                       <CustomButton
                       OnButtonPress={() => props.navigation.navigate("DetailScreen", {screenName})}
                buttonText={"Upload  Image"}
                color={colorConstant.white}
                />
                </View>
    </View>
  )
}

export default ScreenShotImage

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor: "#EDF7FF"
    },
    BottomView: {
        position: "absolute",
        bottom: 0,
        backgroundColor: colorConstant.white,
        height: moderateVerticalScale(90),
        width: Width
    },
})