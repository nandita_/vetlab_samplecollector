import {StackActions, useNavigation} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import React, {useEffect, useState} from 'react';
import DrawerScreen from '../screens/DrawerScreen';
import IntroScreen from '../screens/IntroScreen';
import Login from '../screens/Login';
import Homepage from '../screens/Homepage';
import Profile from '../screens/Profile';
import Document from '../screens/Document';
import Notifications from '../screens/Notifications';
import Dashboard from '../screens/Dashboard';
import Terms_condition from '../screens/Terms_condition';
import Privacy from '../screens/Privacy';
import About from '../screens/About';
import {Width} from '../dimensions/dimensions';
import Account from '../screens/Account';
import DetailScreen from '../screens/DetailScreen';
import SampleCollection from '../screens/SampleCollection';
import Appointment from '../screens/Appointment';
import QRscreen from '../screens/QRscreen';
import {colorConstant} from '../utils/constants';
import ScreenShotImage from '../screens/ScreenShotImage';
import HistoryDetailScreen from '../screens/HistoryDetailScreen';
import PendingHomeCollection from '../screens/PendingHomeCollection';
import HomeCollectionHistory from '../screens/HomeCollectionHistory';

const Stack = createNativeStackNavigator();

const Drawer = createDrawerNavigator();

const DrawerStack = () => {
  return (
    <>
      <Drawer.Navigator
        screenOptions={{
          // drawerPosition: ,
          drawerType: 'slide',
          overlayColor: 'transparent',
          // drawerType: Platform.OS == 'ios' ? 'front' : 'front',
          headerShown: false,
          drawerStyle: {
            // width: Width,
            backgroundColor: colorConstant.buttonColor,
          },
          sceneContainerStyle: {
            backgroundColor: colorConstant.buttonColor,
          },
        }}
        drawerContent={props => <DrawerScreen {...props} num={1} />}>
        <Drawer.Screen name="Home" component={Homepage} />
      </Drawer.Navigator>
    </>
  );
};

function MainStack({navigation}) {
  return (
    <>
      <Stack.Navigator
        initialRouteName={'DrawerStack'}
        screenOptions={{
          headerShown: false,
          animation: 'default',
        }}>
        <Stack.Screen name="DrawerStack" component={DrawerStack} />
        {/* <Stack.Screen name="DrawerScreen" component={DrawerScreen} /> */}
        {/* <Stack.Screen name="Home" component={Homepage} /> */}
        <Stack.Screen name="Account" component={Account} />
        <Stack.Screen name="Profile" component={Profile} />
        <Stack.Screen name="Document" component={Document} />
        <Stack.Screen name="Notification" component={Notifications} />
        <Stack.Screen name="Dashboard" component={Dashboard} />
        <Stack.Screen name="Terms" component={Terms_condition} />
        <Stack.Screen name="Privacy" component={Privacy} />
        <Stack.Screen name="About" component={About} />
        <Stack.Screen
          name="PendingHomeCollection"
          component={PendingHomeCollection}
        />
        <Stack.Screen
          name="HomeCollectionHistory"
          component={HomeCollectionHistory}
        />

        <Stack.Screen name="DetailScreen" component={DetailScreen} />
        <Stack.Screen name="SampleCollection" component={SampleCollection} />
        <Stack.Screen name="Appointment" component={Appointment} />
        <Stack.Screen name="QRscreen" component={QRscreen} />
        <Stack.Screen name="ScreenShotImage" component={ScreenShotImage} />
        <Stack.Screen
          name="HistoryDetailScreen"
          component={HistoryDetailScreen}
        />
      </Stack.Navigator>
    </>
  );
}

export default MainStack;
