import { Image, Platform } from 'react-native';  
  import { createNativeStackNavigator } from '@react-navigation/native-stack';
import IntroScreen from '../screens/IntroScreen';


  
  const Stack = createNativeStackNavigator();
  const IntroStack = (props) => {
  //  console.log("intro");
    return (
      <Stack.Navigator initialRouteName='Intro' screenOptions={{ headerShown: false }} >
         <Stack.Screen name="Intro" component={IntroScreen} />
     </Stack.Navigator>
  
    )
  }
  
  export default IntroStack