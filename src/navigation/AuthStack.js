import {Image, Platform} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';


import Login from '../screens/Login';



const Stack = createNativeStackNavigator();
const AuthStack = props => {
 

  return (
    <Stack.Navigator
      initialRouteName="Login"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="Login" component={Login} />

    </Stack.Navigator>
  );
};

export default AuthStack;
