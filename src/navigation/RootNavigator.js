import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import MainStack from './MainStack';
import { useSelector } from 'react-redux';
import IntroStack from './IntroStack';
import AuthStack from './AuthStack';

const RootNavigator = () => {
  const introStatus=useSelector((state)=>state.counter.counter.introStatus)
  console.log(introStatus , "cjecljdnk");
  const Stack = createNativeStackNavigator();
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      {introStatus == null && (
        <Stack.Screen name="IntroStack" component={IntroStack} />
      )}

      {introStatus == 'auth' && (
        <Stack.Screen name="AuthStack" component={AuthStack} />
      )}

      {introStatus == 'main' && (
        <Stack.Screen name="MainStack" component={MainStack} />
      )}
    </Stack.Navigator>
  );
};

export default RootNavigator;
